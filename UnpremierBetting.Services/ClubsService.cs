﻿using System.Linq.Expressions;
using System.Web.Mvc;
using UnpremierBetting.Models.BindingModels;
using UnpremierBetting.Utilities;
using UnpremierBetting.Utilities.Extensions;

namespace UnpremierBetting.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using UnpremierBetting.Core.Interfaces;
    using UnpremierBetting.Data.Interfaces;
    using UnpremierBetting.Models.EntityModels;
    using UnpremierBetting.Models.Enums;
    using UnpremierBetting.Models.ViewModels.Clubs;
    using UnpremierBetting.Services.Abstract;
    using UnpremierBetting.Services.Interfaces;

    public class ClubsService : Service, IClubsService
    {
        private readonly IClubManager clubManager;

        public ClubsService(IUnpremierBettingData data, IClubManager clubManager)
            : base(data)
        {
            this.clubManager = clubManager;
        }

        //Fills the squads before they're added to the league
        public void CreateSquads(IEnumerable<int> ids)
        {
            var clubs = this.Data.Clubs.Find(c => ids.Contains(c.Id));

            var names = this.Data.Names.GetAll();
            var nationalities = this.Data.Nationalities.GetAll();
            var positions = this.Data.Positions.GetAll();

            if (!clubs.Any() || !positions.Any() ||
                !names.Any() || !nationalities.Any())
            {
                throw new ArgumentException("Seed data is missing from the database.");
            }

            this.clubManager.FillClubSquads(names, nationalities, positions, clubs);

            this.Data.Commit();
        }

        //Only pre-league clubs can be deleted from the database
        public void DeleteSquads(IEnumerable<int> ids)
        {
            var clubs = this.Data.Clubs.Find(c => ids.Contains(c.Id));
            foreach (var club in clubs)
            {
                if (club.Leagues.Any())
                {
                    throw new ArgumentException($"Cannot delete {club.Name}'s squad " +
                                                "because it is in a league already.");
                }

                //TODO: Make a player deletion property
                var squad = club.Squad.ToList();
                squad.ForEach(p => this.Data.MatchPlayers.RemoveRange(p.Matches.ToList()));
                this.Data.Players.RemoveRange(squad);
            }

            this.Data.Commit();
        }

        public ClubDetailsViewModel GetClubDetails(int id)
        {
            var club = this.Data.Clubs.GetById(id);
            if (club == null)
            {
                throw new ArgumentException("No club with this ID was found.");
            }

            var clubVm = Mapper.Map<Club, ClubDetailsViewModel>(club);

            var squad = club.Squad.OrderByDescending(p => p.CalculateOverall()).ToList();
            if (!squad.Any())
            {
                return clubVm;
            }

            var topPlayers = new List<Player>();
            var formationPositions = club.Formations.First().PlayerPositions.OrderBy(f => f.Position.Type);

            foreach (var formationPosition in formationPositions)
            {
                for (int i = 0; i < formationPosition.PlayersCount; i++)
                {
                    var player = squad
                        .FirstOrDefault(p => p.Positions.Contains(formationPosition.Position)) ??
                        squad.First(p => p.GetPositionType() != PositionType.Goalkeeper);

                    topPlayers.Add(player);
                    squad.Remove(player);
                }
            }

            var topPlayerVms = Mapper.Map<IEnumerable<ClubPlayerViewModel>>(topPlayers);
            clubVm.TopPlayers.AddRange(topPlayerVms);

            var benchVms = Mapper.Map<IEnumerable<ClubPlayerViewModel>>(squad);
            clubVm.Bench.AddRange(benchVms);

            var seasonClub = this.GetSeasonClub(club);
            if (seasonClub == null)
            {
                return clubVm;
            }

            var playedMatches = seasonClub.GetMatches()
                .Where(m => m.MatchStatus == MatchStatus.Played)
                .OrderByDescending(m => m.MatchDay).Take(5);

            var playedMatcheVms = Mapper.Map<IEnumerable<ClubMatchViewModel>>(playedMatches);
            clubVm.PlayedMatches.AddRange(playedMatcheVms);

            var upcomingMatches = seasonClub.GetMatches()
                .Where(m => m.MatchStatus == MatchStatus.Upcoming)
                .OrderBy(m => m.MatchDay).Take(5);

            var upcomingMatchVms = Mapper.Map<IEnumerable<ClubMatchViewModel>>(upcomingMatches);
            clubVm.UpcomingMatches.AddRange(upcomingMatchVms);

            return clubVm;
        }

        public IEnumerable<ClubViewModel> GetClubs()
        {
            var clubs = this.Data.Clubs.Find(c => !c.Leagues.Any());

            return Mapper.Map<IEnumerable<ClubViewModel>>(clubs);
        }

        public void AddClubToLeague(LeagueBindingModel leagueBm)
        {
            var league = this.Data.Leagues.GetById(leagueBm.LeagueId);
            if (league == null)
            {
                throw new ArgumentException("League was not found.");
            }

            var clubs = this.Data.Clubs.Find(c => leagueBm.ClubIds.Contains(c.Id));
            foreach (var club in clubs)
            {
                league.Clubs.Add(club);
            }

            this.Data.Commit();
        }
    }
}