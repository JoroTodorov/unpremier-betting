﻿using UnpremierBetting.Utilities.Extensions;

namespace UnpremierBetting.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using UnpremierBetting.Data.Interfaces;
    using UnpremierBetting.Models.EntityModels;
    using UnpremierBetting.Models.ViewModels.Fixtures;
    using UnpremierBetting.Models.ViewModels.Matches;
    using UnpremierBetting.Services.Abstract;
    using UnpremierBetting.Services.Interfaces;
    using UnpremierBetting.Utilities;

    public class MatchesService : Service, IMatchesService
    {
        public MatchesService(IUnpremierBettingData data)
            : base(data)
        {
        }

        public IEnumerable<FixturesViewModel> GetFixtures(int leagueId, int year)
        {
            var season = this.GetSeason(leagueId, year);

            var fixturesMatches = new Dictionary<DateTime, IList<Match>>();

            var seasonClubsMatches = season.Clubs.Select(c => c.GetMatches());
            foreach (var matches in seasonClubsMatches)
            {
                foreach (var match in matches)
                {
                    var matchDay = match.MatchDay;
                    if (!fixturesMatches.ContainsKey(matchDay))
                    {
                        fixturesMatches.Add(matchDay, new List<Match>());
                    }
                    if (fixturesMatches[matchDay].Contains(match))
                    {
                        continue;
                    }

                    fixturesMatches[matchDay].Add(match);
                }
            }

            var fixtureVms = new List<FixturesViewModel>();

            foreach (var fixtureDate in fixturesMatches.Keys.OrderBy(k => k))
            {
                var matchVms = Mapper.Map<IEnumerable<FixtureMatchViewModel>>(fixturesMatches[fixtureDate]);
                var fixtureVm = new FixturesViewModel()
                {
                    MatchDay = fixtureDate,
                    Matches = matchVms
                };

                fixtureVms.Add(fixtureVm);
            }

            return fixtureVms;
        }

        public MatchDetailsViewModel GetMatchDetails(int id)
        {
            var match = this.Data.Matches.GetById(id);

            if (match == null || match.IsDeleted())
            {
                throw new NullReferenceException("Match not found.");
            }

            var matchVm = Mapper.Map<Match, MatchDetailsViewModel>(match);

            return matchVm;
        }
    }
}