﻿namespace UnpremierBetting.Services.Interfaces
{
    using System.Collections.Generic;
    using UnpremierBetting.Models.BindingModels;
    using UnpremierBetting.Models.EntityModels;
    using UnpremierBetting.Models.ViewModels;
    using UnpremierBetting.Models.ViewModels.Bets;

    public interface IBetsService
    {
        bool MatchIsBet(int matchId, string userId);

        IEnumerable<BetViewModel> GetMatchBets(int id);

        void AddMatchBet(int id, string userId);

        Match GetMatchByBet(int betId);

        BetTicketViewModel GetUserTicket(string userId);

        void AddBetTicket(string userId, BetTicketConfirmationBindingModel ticketBm);

        BetTicketConfirmationViewModel GetTicketConfirmation(BetTicketBindingModel ticketBm);

        bool UserHasTheAmount(string userId, decimal amount);

        void RemoveBetFromTicket(int betId, string userId);
    }
}