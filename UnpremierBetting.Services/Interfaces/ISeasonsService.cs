﻿using System.Linq.Expressions;
using UnpremierBetting.Models.ViewModels.Clubs;
using UnpremierBetting.Models.ViewModels.Players;

namespace UnpremierBetting.Services.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using UnpremierBetting.Models.BindingModels;
    using UnpremierBetting.Models.EntityModels;
    using UnpremierBetting.Models.ViewModels;
    using UnpremierBetting.Models.ViewModels.Seasons;

    public interface ISeasonsService
    {
        IEnumerable<LeagueTableClubViewModel> GetLeagueTable(int leagueId, int year);

        IEnumerable<SelectListItem> GetSeasonsList();

        void CreateSeasonMatches(int leagueId, DateTime firstMatchDate);

        void DeleteSeasonMatches(int leagueId, int year);

        void SimulateSeason(int leagueId, int year);

        void CreateLeague(string name);

        IEnumerable<SelectListItem> GetLeaguesList(Expression<Func<League, bool>> predicate = null);
    }
}