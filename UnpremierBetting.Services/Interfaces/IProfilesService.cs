﻿namespace UnpremierBetting.Services.Interfaces
{
    using System.Collections.Generic;
    using UnpremierBetting.Models.ViewModels.Profiles;

    public interface IProfilesService
    {
        IEnumerable<ProfileTicketViewModel> GetUserTickets(string userId);

        decimal GetUserBalance(string userId);
    }
}