﻿namespace UnpremierBetting.Services.Interfaces
{
    using UnpremierBetting.Data.Interfaces;
    using UnpremierBetting.Models.EntityModels;

    public interface IService
    {
        IUnpremierBettingData Data { get; set; }
    }
}