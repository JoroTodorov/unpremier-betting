﻿using System.Collections.Generic;
using System.Linq;

namespace UnpremierBetting.Services.Interfaces
{
    using UnpremierBetting.Models.ViewModels.Players;

    public interface IPlayersService
    {
        PlayerDetailsViewModel GetPlayer(int playerId);

        IDictionary<string, IList<PlayerStatisticViewModel>> GetStats(int leagueId, int year);
    }
}