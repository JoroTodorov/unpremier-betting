﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using UnpremierBetting.Models.BindingModels;

namespace UnpremierBetting.Services.Interfaces
{
    using System.Collections.Generic;
    using UnpremierBetting.Models.EntityModels;
    using UnpremierBetting.Models.ViewModels;
    using UnpremierBetting.Models.ViewModels.Clubs;

    public interface IClubsService
    {
        void CreateSquads(IEnumerable<int> ids);

        void DeleteSquads(IEnumerable<int> ids);

        ClubDetailsViewModel GetClubDetails(int id);

        IEnumerable<ClubViewModel> GetClubs();

        void AddClubToLeague(LeagueBindingModel leagueBm);

        IEnumerable<SelectListItem> GetLeaguesList(Expression<Func<League, bool>> predicate = null);
    }
}