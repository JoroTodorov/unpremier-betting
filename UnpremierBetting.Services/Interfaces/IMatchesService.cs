﻿using System.Linq.Expressions;

namespace UnpremierBetting.Services.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using UnpremierBetting.Models.BindingModels;
    using UnpremierBetting.Models.EntityModels;
    using UnpremierBetting.Models.ViewModels;
    using UnpremierBetting.Models.ViewModels.Bets;
    using UnpremierBetting.Models.ViewModels.Fixtures;
    using UnpremierBetting.Models.ViewModels.Matches;

    public interface IMatchesService
    {
        IEnumerable<FixturesViewModel> GetFixtures(int leagueId, int year);

        IEnumerable<SelectListItem> GetSeasonsList();

        IEnumerable<SelectListItem> GetLeaguesList(Expression<Func<League, bool>> predicate = null);

        MatchDetailsViewModel GetMatchDetails(int id);

    }
}