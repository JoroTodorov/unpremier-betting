﻿using UnpremierBetting.Utilities.Extensions;

namespace UnpremierBetting.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using UnpremierBetting.Data.Interfaces;
    using UnpremierBetting.Models.ViewModels.Profiles;
    using UnpremierBetting.Services.Abstract;
    using UnpremierBetting.Services.Interfaces;

    public class ProfilesService : Service, IProfilesService
    {
        public ProfilesService(IUnpremierBettingData data)
            : base(data)
        {
        }

        public IEnumerable<ProfileTicketViewModel> GetUserTickets(string userId)
        {
            var userTickets = this.Data.Users.GetById(userId).BetTickets.Where(
                t => !t.MatchBets.Any(mb => mb.Match.IsDeleted()));
            var ticketVms = Mapper.Map<IEnumerable<ProfileTicketViewModel>>(userTickets);
            foreach (var ticket in ticketVms)
            {
                float combinedOdds = ticket.Bets.Select(b => b.Odds).Aggregate((b1, b2) => b1 * b2);

                ticket.CombinedOdds = combinedOdds;
                ticket.WinningAmount = ticket.Amount * (decimal)combinedOdds;
            }

            return ticketVms;
        }

        public decimal GetUserBalance(string userId)
        {
            return this.Data.Users.GetById(userId).Balance;
        }
    }
}