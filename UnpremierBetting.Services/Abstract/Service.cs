﻿using System.Linq.Expressions;
using AutoMapper;
using UnpremierBetting.Models.ViewModels.Clubs;

namespace UnpremierBetting.Services.Abstract
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using AutoMapper.QueryableExtensions;
    using UnpremierBetting.Data.Interfaces;
    using UnpremierBetting.Models.EntityModels;
    using UnpremierBetting.Services.Interfaces;

    public abstract class Service : IService
    {
        protected Service(IUnpremierBettingData data)
        {
            this.Data = data;
        }

        public virtual IUnpremierBettingData Data { get; set; }

        public virtual Season GetSeason(int leagueId, int year)
        {
            var league = this.Data.Leagues.GetById(leagueId) ??
                         this.Data.Leagues.GetAll().OrderBy(l => l.Name).First(l => l.Seasons.Any(s => !s.IsDeleted));
            if (league == null)
            {
                throw new ArgumentException("No league with seasons found.");
            }

            var currentSeasonYear = league.Seasons.Where(s => !s.IsDeleted)
                .Select(s => s.FirstMatchDay.Year).OrderByDescending(s => s).FirstOrDefault();
            if (currentSeasonYear == 0)
            {
                throw new ArgumentException("No seasons found.");
            }

            year = year == 0 ? currentSeasonYear : year;
            var season = league.Seasons.FirstOrDefault(s => s.FirstMatchDay.Year == year && !s.IsDeleted) ??
                         this.Data.Seasons.First(s => s.FirstMatchDay.Year == currentSeasonYear && !s.IsDeleted);

            return season;
        }

        public virtual SeasonClub GetSeasonClub(Club club)
        {
            var allSeasons = club.Seasons.Where(s => !s.Season.IsDeleted);
            if (!allSeasons.Any())
            {
                return null;
            }

            int seasonYear = allSeasons.Max(s => s.Season.FirstMatchDay.Year);

            var seasonClub = club.Seasons.First(
                s => !s.Season.IsDeleted && s.Season.FirstMatchDay.Year == seasonYear);

            return seasonClub;
        }

        public IEnumerable<SelectListItem> GetLeaguesList(Expression<Func<League, bool>> predicate = null)
        {
            var allLeagues = this.Data.Leagues
                .Find(predicate ?? (l => true))
                .OrderBy(l => l.Name)
                .ProjectTo<SelectListItem>();

            return allLeagues;
        }

        public IEnumerable<SelectListItem> GetSeasonsList()
        {
            var allSeasons = this.Data.Seasons
                .Find(s => !s.IsDeleted)
                .OrderByDescending(s => s.FirstMatchDay.Year)
                .ProjectTo<SelectListItem>();

            return allSeasons;
        }
    }
}