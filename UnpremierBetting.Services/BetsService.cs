﻿using UnpremierBetting.Utilities.Extensions;

namespace UnpremierBetting.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using UnpremierBetting.Data.Interfaces;
    using UnpremierBetting.Models.BindingModels;
    using UnpremierBetting.Models.EntityModels;
    using UnpremierBetting.Models.Enums;
    using UnpremierBetting.Models.ViewModels.Bets;
    using UnpremierBetting.Services.Abstract;
    using UnpremierBetting.Services.Interfaces;

    public class BetsService : Service, IBetsService
    {
        public BetsService(IUnpremierBettingData data)
            : base(data)
        {
        }

        public bool MatchIsBet(int matchId, string userId)
        {
            var user = this.Data.Users.GetById(userId);
            if (user == null)
            {
                return false;
            }

            return user.PendingTicketMatches.Any(m => m.Match.Id == matchId);
        }

        public IEnumerable<BetViewModel> GetMatchBets(int id)
        {
            var matchBets = this.Data.Matches.GetById(id).MatchBets.OrderBy(mb => mb.Type);
            var matchBetVms = Mapper.Map<IEnumerable<BetViewModel>>(matchBets);

            return matchBetVms;
        }

        public void AddMatchBet(int id, string userId)
        {
            var user = this.Data.Users.GetById(userId);
            var matchBet = this.Data.MatchBets.GetById(id);

            user.PendingTicketMatches.Add(matchBet);
            this.Data.Commit();
        }

        public Match GetMatchByBet(int betId)
        {
            return this.Data.MatchBets.GetById(betId).Match;
        }

        public BetTicketViewModel GetUserTicket(string userId)
        {
            var userTicket = this.Data.Users.GetById(userId).PendingTicketMatches
              .Where(b => !b.Match.IsDeleted())
              .OrderBy(b => b.Match.MatchDay);

            var betsVm = Mapper.Map<ICollection<BetDetailsViewModel>>(userTicket);

            float combinedOdds = 0;

            var odds = userTicket.Select(b => b.Odds);
            if (odds.Any())
            {
                combinedOdds = odds.Aggregate((b1, b2) => b1 * b2);
            }

            var ticketVm = new BetTicketViewModel()
            {
                CombinedOdds = combinedOdds,
                Bets = betsVm
            };

            return ticketVm;
        }

        public void AddBetTicket(string userId, BetTicketConfirmationBindingModel ticketBm)
        {
            var user = this.Data.Users.GetById(userId);
            var matchBets = user.PendingTicketMatches
                .Where(b => !b.Match.IsDeleted()).ToArray();

            var userTicket = new UserTicket()
            {
                User = user,
                MatchBets = matchBets,
                Amount = ticketBm.BettingAmount,
                Status = TicketStatus.Pending
            };

            user.BetTickets.Add(userTicket);
            user.Balance -= ticketBm.BettingAmount;
            
            this.Data.Commit();
        }

        public BetTicketConfirmationViewModel GetTicketConfirmation(BetTicketBindingModel ticketBm)
        {
            var ticketVm = Mapper.Map<BetTicketConfirmationViewModel>(ticketBm);

            return ticketVm;
        }

        public bool UserHasTheAmount(string userId, decimal amount)
        {
            var user = this.Data.Users.GetById(userId);

            return user.Balance >= amount;
        }

        public void RemoveBetFromTicket(int betId, string userId)
        {
            var userTicket = this.Data.Users.GetById(userId).PendingTicketMatches;
            var bet = userTicket.FirstOrDefault(t => t.Id == betId);
            if (bet != null)
            {
                userTicket.Remove(bet);
            }

            this.Data.Commit();
        }
    }
}