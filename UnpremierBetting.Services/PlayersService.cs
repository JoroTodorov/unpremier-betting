﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using UnpremierBetting.Models.Enums;
using UnpremierBetting.Utilities;
using UnpremierBetting.Utilities.Extensions;

namespace UnpremierBetting.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using UnpremierBetting.Data.Interfaces;
    using UnpremierBetting.Models.EntityModels;
    using UnpremierBetting.Models.ViewModels.Players;
    using UnpremierBetting.Services.Abstract;
    using UnpremierBetting.Services.Interfaces;

    public class PlayersService : Service, IPlayersService
    {
        public PlayersService(IUnpremierBettingData data)
            : base(data)
        {
        }

        public PlayerDetailsViewModel GetPlayer(int playerId)
        {
            var player = this.Data.Players.GetById(playerId);
            if (player == null)
            {
                throw new NullReferenceException("Player not found.");
            }

            var playerVm = Mapper.Map<PlayerDetailsViewModel>(player);

            var abilities = player.Ability.GetType().GetProperties()
                .Where(p => p.PropertyType == typeof(int) && p.Name != "Id");

            var abilityVms = new List<PlayerAbilitiesViewModel>();

            foreach (var ability in abilities)
            {
                int abilityValue = (int)ability.GetValue(player.Ability);

                string color =
                    abilityValue < 40 ? "danger"
                        : abilityValue < 70 ? "warning"
                            : abilityValue < 90 ? "success" : "default";

                string abilityName = ability.Name;
                if (ability.GetCustomAttribute<DisplayAttribute>() != null)
                {
                    abilityName = ability.GetCustomAttribute<DisplayAttribute>().Name;
                }

                var abilityVm = new PlayerAbilitiesViewModel()
                {
                    AbilityName = abilityName,
                    AbilityValue = abilityValue,
                    AbilityColor = color
                };

                abilityVms.Add(abilityVm);
            }

            playerVm.MainAbilities = abilityVms.OrderByDescending(a => a.AbilityValue).Take(4);
            playerVm.SecondaryAbilities = abilityVms.Where(a => !playerVm.MainAbilities.Contains(a));

            var homeMatches = player.Matches.Where(m => m.Side == PlayerSide.Home)
                .Select(m => m.Match).Where(m => !m.IsDeleted());
            var awayMatches = player.Matches.Where(m => m.Side == PlayerSide.Away)
                .Select(m => m.Match).Where(m => !m.IsDeleted());

            var playerMatches = new List<Match>(homeMatches);
            playerMatches.AddRange(awayMatches);
            playerMatches = playerMatches.OrderByDescending(m => m.MatchDay).Take(5).ToList();

            var playerMatchVms = Mapper.Map<IEnumerable<PlayerMatchViewModel>>(playerMatches);

            playerVm.PlayedMatches = playerMatchVms;

            return playerVm;
        }

        public IDictionary<string, IList<PlayerStatisticViewModel>> GetStats(int leagueId, int year)
        {
            var stats = new Dictionary<string, IList<PlayerStatisticViewModel>>
            {
                {"Goals", new List<PlayerStatisticViewModel>()},
                {"Assists", new List<PlayerStatisticViewModel>()}
            };

            var topGoalStatistics = this.GetSeason(leagueId, year).Clubs
                .Select(c => c.GetStatistics(StatType.Goal))
                .SelectMany(cs => cs).ToDictionary(ps => ps.Key, ps => ps.Value)
                .OrderByDescending(
                    ps => ps.Value.Count(s => s.StatType == StatType.Goal)).Take(10);

            int pos = 1;
            foreach (var goalscorerStats in topGoalStatistics)
            {
                int picSize = pos <= 3 ? 100 : 50;

                var goalsVm = new PlayerStatisticViewModel()
                {
                    Position = pos,
                    PlayerId = goalscorerStats.Key.Id,
                    PlayerName = goalscorerStats.Key.GetFullName(),
                    PictureId = goalscorerStats.Key.PictureId,
                    PictureSize = picSize,
                    StatCount = goalscorerStats.Value.Count
                };

                stats["Goals"].Add(goalsVm);
                pos++;
            }

            var topAssistStatistics = this.GetSeason(leagueId, year).Clubs
                .Select(c => c.GetStatistics(StatType.Assist))
                .SelectMany(cs => cs).ToDictionary(ps => ps.Key, ps => ps.Value)
                .OrderByDescending(
                    ps => ps.Value.Count(s => s.StatType == StatType.Assist)).Take(10);

            pos = 1;

            foreach (var assisterStats in topAssistStatistics)
            {
                int picSize = pos <= 3 ? 100 : 50;

                var assistsVm = new PlayerStatisticViewModel()
                {
                    Position = pos,
                    PlayerId = assisterStats.Key.Id,
                    PlayerName = assisterStats.Key.GetFullName(),
                    PictureId = assisterStats.Key.PictureId,
                    PictureSize = picSize,
                    StatCount = assisterStats.Value.Count
                };

                stats["Assists"].Add(assistsVm);
                pos++;
            }

            return stats;
        }
    }
}