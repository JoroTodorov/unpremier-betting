﻿using UnpremierBetting.Models.BindingModels;
using UnpremierBetting.Models.Enums;
using UnpremierBetting.Models.ViewModels.Players;
using UnpremierBetting.Utilities.Extensions;

namespace UnpremierBetting.Services
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Validation;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using AutoMapper;
    using AutoMapper.QueryableExtensions;
    using Microsoft.AspNet.Identity;
    using UnpremierBetting.Core.Interfaces;
    using UnpremierBetting.Data.Interfaces;
    using UnpremierBetting.Models.EntityModels;
    using UnpremierBetting.Models.ViewModels.Seasons;
    using UnpremierBetting.Services.Abstract;
    using UnpremierBetting.Services.Interfaces;
    using Constants = UnpremierBetting.Utilities.Constants;

    public class SeasonsService : Service, ISeasonsService
    {
        private readonly IMatchSystem matchSystem;

        public SeasonsService(IUnpremierBettingData data, IMatchSystem matchSystem)
            : base(data)
        {
            this.matchSystem = matchSystem;
        }

        public void CreateLeague(string name)
        {
            if (this.Data.Leagues.Has(l => l.Name == name))
            {
                throw new ArgumentException("League with this name already exists.");
            }

            var league = new League { Name = name };
            this.Data.Leagues.Add(league);
            this.Data.Commit();
        }

        public void CreateSeasonMatches(int leagueId, DateTime firstMatchDate)
        {
            var league = this.Data.Leagues.GetById(leagueId);
            var season = league.Seasons.FirstOrDefault(
                s => s.FirstMatchDay.Year == firstMatchDate.Year && !s.IsDeleted);

            if (season != null)
            {
                throw new ArgumentException("Matches for this season have been already generated.");
            }

            season = new Season() { FirstMatchDay = firstMatchDate, EndMatchDate = firstMatchDate};
            league.Seasons.Add(season);

            foreach (var club in league.Clubs)
            {
                if (!club.Squad.Any())
                {
                    throw new ArgumentException("The club dosen't have a squad.");
                }
                var seasonClub = new SeasonClub()
                {
                    Season = season,
                    Club = club
                };

                this.Data.SeasonClubs.Add(seasonClub);
            }

            this.matchSystem.CreateMatches(season);

            this.Data.Commit();
        }

        public void DeleteSeasonMatches(int leagueId, int year)
        {
            var league = this.Data.Leagues.GetById(leagueId);
            var season = league.Seasons.FirstOrDefault(
                s => s.FirstMatchDay.Year == year && !s.IsDeleted);

            if (season == null)
            {
                throw new ArgumentException("A season hasn't been generated for this current year.");
            }

            season.IsDeleted = true;

            this.Data.Commit();
        }

        public void SimulateSeason(int leagueId, int year)
        {
            var league = this.Data.Leagues.GetById(leagueId);
            var season = league.Seasons.FirstOrDefault(
                s => s.FirstMatchDay.Year == year && !s.IsDeleted);

            if (season == null)
            {
                throw new ArgumentException("A season hasn't been generated for this current year.");
            }

            //For testing purposes the current date is the one created for the season's first match date
            Constants.ServerTime = season.FirstMatchDay;
            var delay = new TimeSpan(0, 0, 5);

            Task.Run(() =>
            {
                while (true)
                {
                    var formPositions = this.Data.FormationPositions.ToArray;
                    var userTickets = this.Data.UserTickets.ToArray;
                    this.matchSystem.SimulateMatches(
                        Constants.ServerTime,
                        formPositions, season.Clubs,
                        userTickets);

                    this.Data.Commit();

                    Constants.ServerTime = Constants.ServerTime.AddDays(1);
                    if (season.IsDeleted || Constants.ServerTime > season.EndMatchDate)
                    {
                        break;
                    }

                    Thread.Sleep(delay);
                }
            });
        }

        public IEnumerable<LeagueTableClubViewModel> GetLeagueTable(int leagueId, int year)
        {
            var seasonClubs = this.GetSeason(leagueId, year).Clubs;
            var clubTableVms = Mapper.Map<IEnumerable<LeagueTableClubViewModel>>(seasonClubs).OrderBy(c => c.Position);

            return clubTableVms;
        }
    }
}