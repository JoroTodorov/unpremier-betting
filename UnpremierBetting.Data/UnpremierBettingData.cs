﻿namespace UnpremierBetting.Data
{
    using UnpremierBetting.Data.Interfaces;
    using UnpremierBetting.Models;
    using UnpremierBetting.Models.EntityModels;
    using UnpremierBetting.Models.EntityModels.Identity;

    public class UnpremierBettingData : IUnpremierBettingData
    {
        private readonly IDbContext context;

        private IRepository<ApplicationUser> users;

        private IRepository<UserTicket> userTickets;

        private IRepository<Season> seasons;

        private IRepository<Player> players;

        private IRepository<Ability> abilities;

        private IRepository<Match> matches;

        private IRepository<MatchBet> matchBets;

        private IRepository<MatchPlayer> matchPlayers;

        private IRepository<Club> clubs;

        private IRepository<League> leagues;

        private IRepository<SeasonClub> seasonClubs;

        private IRepository<Statistic> statistics;

        private IRepository<Position> positions;

        private IRepository<Formation> formation;

        private IRepository<FormationPosition> formationPositions;

        private IRepository<Stadium> stadiums;

        private IRepository<Nationality> nationalities;

        private IRepository<Name> names;

        public UnpremierBettingData(IDbContext context)
        {
            this.context = context;
        }

        public IRepository<ApplicationUser> Users
        => this.users ?? (this.users = new Repository<ApplicationUser>(this.context));

        public IRepository<Season> Seasons
            => this.seasons ?? (this.seasons = new Repository<Season>(this.context));

        public IRepository<Ability> Abilities
            => this.abilities ?? (this.abilities = new Repository<Ability>(this.context));

        public IRepository<Player> Players
                   => this.players ?? (this.players = new Repository<Player>(this.context));

        public IRepository<Match> Matches
            => this.matches ?? (this.matches = new Repository<Match>(this.context));

        public IRepository<MatchBet> MatchBets
       => this.matchBets ?? (this.matchBets = new Repository<MatchBet>(this.context));

        public IRepository<MatchPlayer> MatchPlayers
            => this.matchPlayers ?? (this.matchPlayers = new Repository<MatchPlayer>(this.context));

        public IRepository<UserTicket> UserTickets
     => this.userTickets ?? (this.userTickets = new Repository<UserTicket>(this.context));

        public IRepository<Club> Clubs
            => this.clubs ?? (this.clubs = new Repository<Club>(this.context));

        public IRepository<League> Leagues
            => this.leagues ?? (this.leagues = new Repository<League>(this.context));

        public IRepository<SeasonClub> SeasonClubs
       => this.seasonClubs ?? (this.seasonClubs = new Repository<SeasonClub>(this.context));

        public IRepository<Statistic> Statistics
            => this.statistics ?? (this.statistics = new Repository<Statistic>(this.context));

        public IRepository<Stadium> Stadiums
            => this.stadiums ?? (this.stadiums = new Repository<Stadium>(this.context));

        public IRepository<Position> Positions
            => this.positions ?? (this.positions = new Repository<Position>(this.context));

        public IRepository<Formation> Formations
         => this.formation ?? (this.formation = new Repository<Formation>(this.context));

        public IRepository<FormationPosition> FormationPositions
         => this.formationPositions ?? (this.formationPositions = new Repository<FormationPosition>(this.context));

        public IRepository<Nationality> Nationalities
            => this.nationalities ?? (this.nationalities = new Repository<Nationality>(this.context));

        public IRepository<Name> Names
            => this.names ?? (this.names = new Repository<Name>(this.context));

        public void Commit()
        {
            this.context.SaveChanges();
        }

        public void Dispose()
        {
            this.context.Dispose();
        }
    }
}