namespace UnpremierBetting.Data
{
    using System.Data.Entity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using UnpremierBetting.Data.Interfaces;
    using UnpremierBetting.Data.Migrations;
    using UnpremierBetting.Models.EntityModels;
    using UnpremierBetting.Models.EntityModels.Identity;

    public class UnpremierBettingContext : IdentityDbContext<ApplicationUser>, IDbContext
    {
        public UnpremierBettingContext()
            : base("UnpremierBetting")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<UnpremierBettingContext, Configuration>());
        }

        public IDbSet<Club> Clubs { get; set; }

        public IDbSet<Player> Players { get; set; }

        public IDbSet<League> Leagues { get; set; }

        public IDbSet<MatchBet> MatchBets { get; set; }

        public IDbSet<UserTicket> UserTickets { get; set; }

        public IDbSet<SeasonClub> SeasonClubs { get; set; }

        public IDbSet<Position> Positions { get; set; }

        public IDbSet<Formation> Formations { get; set; }

        public IDbSet<FormationPosition> FormationPositions { get; set; }

        public IDbSet<Stadium> Stadiums { get; set; }

        public IDbSet<Nationality> Nationalities { get; set; }

        public IDbSet<Name> Names { get; set; }

        public static UnpremierBettingContext Create()
        {
            return new UnpremierBettingContext();
        }

        public new IDbSet<T> Set<T>() where T : class
        {
            return base.Set<T>();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Player>()
                .HasMany(p => p.Positions)
                .WithMany(p => p.Players)
                .Map(m => m.ToTable("PlayerPositions"));

            modelBuilder.Entity<Player>()
                .HasRequired(p => p.Ability)
                .WithRequiredDependent(a => a.Player);

            modelBuilder.Entity<Club>()
                .HasMany(c => c.Formations)
                .WithMany(f => f.Teams)
                .Map(m => m.ToTable("ClubFormations"));

            modelBuilder.Entity<MatchBet>()
                .HasMany(mb => mb.UserTickets)
                .WithMany(ub => ub.MatchBets)
                .Map(m => m.ToTable("UserTicketBets"));

            modelBuilder.Entity<MatchBet>()
                .HasMany(mb => mb.PendingTicketUsers)
                .WithMany(ub => ub.PendingTicketMatches)
                .Map(m => m.ToTable("PendingUserTickets"));

            base.OnModelCreating(modelBuilder);
        }
    }
}