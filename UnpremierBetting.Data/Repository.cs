﻿using System.Collections.Generic;

namespace UnpremierBetting.Data
{
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using UnpremierBetting.Data.Interfaces;

    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly IDbSet<T> modelTable;

        public Repository(IDbContext context)
        {
            this.modelTable = context.Set<T>();
        }

        public T[] ToArray => this.GetAll().ToArray();

        public int Count => this.GetAll().Count();

        public void Add(T entity)
        {
            this.modelTable.Add(entity);
        }

        public void AddRange(IQueryable<T> entities)
        {
            foreach (var entity in entities)
            {
                this.modelTable.Add(entity);
            }
        }

        public void Remove(T entity)
        {
            this.modelTable.Remove(entity);
        }

        public void RemoveRange(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                this.modelTable.Remove(entity);
            }
        }

        public T GetById(object id)
        {
            return this.modelTable.Find(id);
        }

        public IQueryable<T> GetAll()
        {
            return this.modelTable;
        }

        public bool Has(Expression<Func<T, bool>> predicate)
        {
            return this.modelTable.Any(predicate);
        }

        public T First(Expression<Func<T, bool>> predicate)
        {
            return this.modelTable.FirstOrDefault(predicate);
        }

        public IQueryable<T> Find(Expression<Func<T, bool>> predicate)
        {
            return this.modelTable.Where(predicate);
        }
    }
}