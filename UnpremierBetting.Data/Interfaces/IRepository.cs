﻿using System.Collections.Generic;

namespace UnpremierBetting.Data.Interfaces
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    public interface IRepository<T>
    {
        T[] ToArray { get; }

        int Count { get; }

        void Add(T entity);

        void AddRange(IQueryable<T> entities);

        void Remove(T entity);

        void RemoveRange(IEnumerable<T> entities);

        T GetById(object id);

        IQueryable<T> GetAll();

        bool Has(Expression<Func<T, bool>> predicate);

        T First(Expression<Func<T, bool>> predicate);

        IQueryable<T> Find(Expression<Func<T, bool>> predicate);
    }
}