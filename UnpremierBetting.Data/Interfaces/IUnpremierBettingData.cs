﻿namespace UnpremierBetting.Data.Interfaces
{
    using System;
    using UnpremierBetting.Models;
    using UnpremierBetting.Models.EntityModels;
    using UnpremierBetting.Models.EntityModels.Identity;

    public interface IUnpremierBettingData : IDisposable
    {
        IRepository<ApplicationUser> Users { get; }

        IRepository<Season> Seasons { get; }

        IRepository<Ability> Abilities { get; }

        IRepository<Player> Players { get; }

        IRepository<Match> Matches { get; }

        IRepository<MatchBet> MatchBets { get; }

        IRepository<MatchPlayer> MatchPlayers { get; }

        IRepository<Club> Clubs { get; }

        IRepository<League> Leagues { get; }

        IRepository<SeasonClub> SeasonClubs { get; }

        IRepository<Statistic> Statistics { get; }

        IRepository<Stadium> Stadiums { get; }

        IRepository<Position> Positions { get; }

        IRepository<Formation> Formations { get; }

        IRepository<FormationPosition> FormationPositions { get; }

        IRepository<Nationality> Nationalities { get; }

        IRepository<Name> Names { get; }

        IRepository<UserTicket> UserTickets { get; }

        void Commit();
    }
}