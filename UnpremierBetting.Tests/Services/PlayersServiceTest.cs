﻿namespace UnpremierBetting.Tests.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using UnpremierBetting.Data;
    using UnpremierBetting.Data.Interfaces;
    using UnpremierBetting.Models.EntityModels;
    using UnpremierBetting.Models.ViewModels.Players;
    using UnpremierBetting.Services;
    using UnpremierBetting.Services.Interfaces;
    using Match = UnpremierBetting.Models.EntityModels.Match;

    [TestClass]
    public class PlayersServiceTest
    {
        private IUnpremierBettingData data;
        private IPlayersService service;

        [TestInitialize]
        public void Initialize()
        {
            var ctx = new UnpremierBettingContext();
            this.data = new UnpremierBettingData(ctx);
            this.service = new PlayersService(this.data);

            this.ConfigureMapper();
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetPlayer_EmptyPlayerRepo_ReturnNullException()
        {
            int playerId = 1;

            try
            {
                this.service.GetPlayer(playerId);
                Assert.Fail("Player should't be returned from an empty repo.");
            }
            catch (Exception)
            {
                throw new NullReferenceException();
            }
        }

        [TestMethod]
        public void GetPlayer_AddPlayerToRepo_ReturnValidAbilities()
        {
            var player = this.CreatePlayer();

            this.data.Players.Add(player);
            var playerVm = this.service.GetPlayer(player.Id);

            Assert.AreEqual(player.Ability.AttackingLevel, playerVm.Abilities.Attacking);
            Assert.AreEqual(player.Ability.DefensiveLevel, playerVm.Abilities.Defending);
            Assert.AreEqual(player.Ability.GoalkeepingLevel, playerVm.Abilities.Goalkeeping);
        }

        [TestMethod]
        public void GetPlayer_AddPlayerToRepo_ReturnValidMatches()
        {
            var player = this.CreatePlayer();

            this.data.Players.Add(player);

            var playerVm = this.service.GetPlayer(player.Id);

            Assert.IsTrue(player.HomeMatches.All(
                m => playerVm.PlayedMatches.Any(
                    pm => pm.HomeTeam == m.HomeSeasonTeam.Name && pm.AwayTeam == m.AwaySeasonTeam.Name)));

            Assert.IsTrue(player.AwayMatches.All(
                m => playerVm.PlayedMatches.Any(
                    pm => pm.HomeTeam == m.HomeSeasonTeam.Name && pm.AwayTeam == m.AwaySeasonTeam.Name)));
        }

        [TestMethod]
        public void GetPlayer_AddPlayerToRepo_ReturnValidPlayerData()
        {
            var player = this.CreatePlayer();

            this.data.Players.Add(player);

            var playerVm = this.service.GetPlayer(player.Id);

            Assert.AreEqual(player.Club.Name, playerVm.Club);
            Assert.AreEqual(player.Nationality.Name, playerVm.Nationality);
        }

        [TestMethod]
        public void GetPlayer_AddPlayerToRepo_ReturnValidPositions()
        {
            var player = this.CreatePlayer();

            this.data.Players.Add(player);

            var playerVm = this.service.GetPlayer(player.Id);

            Assert.IsTrue(player.Positions.All(pos => playerVm.Positions.Contains(pos.Id)));
        }

        private Player CreatePlayer()
        {
            var nationality = new Nationality() { Name = "Russia" };
            var positions = new List<Position> { new Position() { Id = "CF" } };

            int playerId = 1;

            var ability = new Ability()
            {
                AttackingLevel = 10,
                DefensiveLevel = 20,
                GoalkeepingLevel = 30
            };

            var club = new Club() { Name = "Barsik" };
            var opponentClub = new Club() { Name = "Enemy" };

            var homeMatch = new Match()
            {
                HomeTeam = club,
                AwayTeam = opponentClub
            };

            var awayMatch = new Match()
            {
                HomeTeam = opponentClub,
                AwayTeam = club
            };

            var matches = new List<Match> { homeMatch, awayMatch };

            var player = new Player()
            {
                Id = playerId,
                Club = club,
                Nationality = nationality,
                Positions = positions,
                Ability = ability,
                HomeMatches = matches,
                AwayMatches = matches
            };

            return player;
        }

        private void ConfigureMapper()
        {
            Mapper.Initialize(map =>
            {
                map.CreateMap<Player, PlayerDetailsViewModel>()
                    .ForMember(vm => vm.Club, model => model.MapFrom(prop => prop.Club.Name))
                    .ForMember(vm => vm.Nationality, model => model.MapFrom(prop => prop.Nationality.Name))
                    .ForMember(vm => vm.Positions, model => model.MapFrom(prop => prop.Positions.Select(p => p.Id)));

                map.CreateMap<Match, PlayerMatchViewModel>()
                 .ForMember(vm => vm.HomeTeam, model => model.MapFrom(prop => prop.HomeTeam.Name))
                 .ForMember(vm => vm.AwayTeam, model => model.MapFrom(prop => prop.AwayTeam.Name));

                map.CreateMap<Ability, PlayerAbilitiesViewModel>()
                    .ForMember(vm => vm.Attacking, model => model.MapFrom(prop => prop.AttackingLevel))
                    .ForMember(vm => vm.Defending, model => model.MapFrom(prop => prop.DefensiveLevel))
                    .ForMember(vm => vm.Goalkeeping, model => model.MapFrom(prop => prop.GoalkeepingLevel));
            });
        }
    }
}