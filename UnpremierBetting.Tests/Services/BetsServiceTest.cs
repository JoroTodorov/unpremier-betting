﻿namespace UnpremierBetting.Tests.Services
{
    using System;
    using System.Linq;
    using AutoMapper;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using UnpremierBetting.Data;
    using UnpremierBetting.Data.Interfaces;
    using UnpremierBetting.Models.BindingModels;
    using UnpremierBetting.Models.EntityModels;
    using UnpremierBetting.Models.EntityModels.Identity;
    using UnpremierBetting.Models.Enums;
    using UnpremierBetting.Models.ViewModels.Bets;
    using UnpremierBetting.Services;
    using Match = UnpremierBetting.Models.EntityModels.Match;

    [TestClass]
    public class BetsServiceTest
    {
        private UnpremierBettingContext ctx;
        private IUnpremierBettingData data;
        private BetsService service;
        private ApplicationUser user;

        [TestInitialize]
        public void Initialize()
        {
            this.ctx = new UnpremierBettingContext();
            this.data = new UnpremierBettingData(this.ctx);
            this.service = new BetsService(this.data);

            this.user = new ApplicationUser()
            {
                Id = "Harabeishio"
            };

            this.data.Users.Add(this.user);

            this.ConfigureMapper();
        }

        [TestMethod]
        public void MatchIsBet_NoMatchesAdded_ReturnsFalse()
        {
            var matchBet = this.CreateMatchBet(56, 19);
            this.user.PendingTicketMatches.Add(matchBet);

            bool isBet = this.service.MatchIsBet(matchBet.Id, this.user.Id);

            Assert.IsFalse(isBet, "Match shouldn't be considered bet.");
        }

        [TestMethod]
        public void MatchIsBet_NoMatchesAdded_ReturnsTrue()
        {
            var matchBet = this.CreateMatchBet(32, 89);
            this.user.PendingTicketMatches.Add(matchBet);

            bool isBet = this.service.MatchIsBet(matchBet.Match.Id, this.user.Id);

            Assert.IsTrue(isBet, "Match should be considered bet.");
        }


        [TestMethod]
        public void GetMatchBets_MatchBetsAdded_ReturnsValidData()
        {
            int matchId = 12;
            var bet = this.CreateMatchBet(23, matchId);

            this.data.MatchBets.Add(bet);

            var matchBetVms = this.service.GetMatchBets(matchId);

            Assert.AreEqual(bet.Odds, matchBetVms.First().Odds, "Odds should be equal.");
            Assert.AreEqual(bet.Type.ToString(), matchBetVms.First().Type, "Type should be the same.");
        }

        private UserTicket CreateTicket()
        {
            var club = new Club()
            {
                Name = "Assholes"
            };

            var opponentClub = new Club()
            {
                Name = "OtherAssholes"
            };

            var match = new Match()
            {
                Id = 56,
                HomeTeam = club,
                HomeGoals = 1,
                AwayGoals = 1,
                AwayTeam = opponentClub,
                MatchDay = DateTime.Today
            };

            var matchBet = new MatchBet()
            {
                Type = BetType.Home,
                Match = match,
                Odds = 1.65f
            };

            var userTicket = new UserTicket()
            {
                Amount = 1000,
                Status = TicketStatus.Pending,
            };

            return userTicket;
        }

        private MatchBet CreateMatchBet(int id, int matchId)
        {
            var match = new Match()
            {
                Id = matchId
            };

            var matchBet = new MatchBet()
            {
                Id = id,
                Match = match,
                Odds = 2.5f,
                Type = BetType.Draw
            };

            return matchBet;
        }

        private void ConfigureMapper()
        {
            Mapper.Initialize(map =>
            {
                map.CreateMap<MatchBet, BetViewModel>()
                    .ForMember(vm => vm.Type, model => model.MapFrom(prop => prop.Type.ToString()));

                map.CreateMap<MatchBet, BetDetailsViewModel>()
                    .ForMember(vm => vm.Type, model => model.MapFrom(prop => prop.Type.ToString()))
                    .ForMember(vm => vm.HomeTeam, model => model.MapFrom(prop => prop.Match.HomeTeam.Name))
                    .ForMember(vm => vm.AwayTeam, model => model.MapFrom(prop => prop.Match.AwayTeam.Name))
                    .ForMember(vm => vm.MatchDate, model => model.MapFrom(prop => prop.Match.MatchDay));

                map.CreateMap<BetTicketBindingModel, BetTicketConfirmationViewModel>()
                    .ForMember(vm => vm.WinningAmount,
                        model => model.MapFrom(prop => (decimal)prop.CombinedOdds * prop.BettingAmount));

            });
        }
    }
}