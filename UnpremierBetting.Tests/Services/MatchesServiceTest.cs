﻿namespace UnpremierBetting.Tests.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using UnpremierBetting.Data;
    using UnpremierBetting.Data.Interfaces;
    using UnpremierBetting.Models.EntityModels;
    using UnpremierBetting.Models.Enums;
    using UnpremierBetting.Models.ViewModels.Fixtures;
    using UnpremierBetting.Models.ViewModels.Matches;
    using UnpremierBetting.Services;
    using Match = UnpremierBetting.Models.EntityModels.Match;

    [TestClass]
    public class MatchesServiceTest
    {
        private IUnpremierBettingData data;
        private Mock<MatchesService> mockedService;
        private Season season;

        [TestInitialize]
        public void Initialize()
        {
            var ctx = new UnpremierBettingContext();
            this.data = new UnpremierBettingData(ctx);
            this.mockedService = new Mock<MatchesService>(this.data);

            this.season = new Season()
            {
                FirstMatchDay = new DateTime(2017, 1, 1),
                EndMatchDate = new DateTime(2018, 1, 1)
            };

            this.mockedService.Setup(s => s.GetSeason(It.IsAny<int>())).Returns(this.season);

            this.ConfigureMapper();
        }

        [TestMethod]
        public void GetFixtures_EmptyMatchesRepo_ShouldNotReturnFixtures()
        {
            var fixtureVms = this.mockedService.Object.GetFixtures(DateTime.Today.Year);

            Assert.IsTrue(!fixtureVms.Any(), "The fixtures' list should be empty.");
        }

        [TestMethod]
        public void GetFixtures_AddMatchToRepo_ReturnValidMatchData()
        {
            var match = this.CreateMatch();
            this.data.Matches.Add(match);

            var fixtureVms = this.mockedService.Object.GetFixtures(this.season.FirstMatchDay.Year);

            Assert.IsTrue(fixtureVms.Any(f => f.MatchDay == match.MatchDay), "Match day from the match is missing.");

            Assert.IsTrue(
                fixtureVms.Any(
                    f => f.Matches.Any(
                        m => m.HomeTeam == match.HomeTeam.Name)), "Home team name is missing.");

            Assert.IsTrue(
                fixtureVms.Any(
                    f => f.Matches.Any(
                        m => m.AwayTeam == match.AwayTeam.Name)), "Away team name is missing.");

            Assert.IsTrue(
                fixtureVms.Any(
                    f => f.Matches.Any(
                        m => m.MatchStatus == match.MatchStatus)), "Match status is missing.");
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetMatchDetails_EmptyMatchesRepo_ShouldThrowException()
        {
            try
            {
                this.mockedService.Object.GetMatchDetails(1);
            }
            catch (Exception)
            {
                throw new NullReferenceException();
            }
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetMatchDetails_AddMatchToRepo_ReturnValidMatchData()
        {
            var match = this.CreateMatch();
            this.data.Matches.Add(match);

            var matchVm = this.mockedService.Object.GetMatchDetails(match.Id);

            Assert.IsTrue(matchVm.HomeTeam == match.HomeTeam.Name, "Home team names should match.");
            Assert.IsTrue(matchVm.AwayTeam == match.AwayTeam.Name, "Away team names should match.");
            Assert.IsTrue(matchVm.BetsExpired == (match.MatchStatus == MatchStatus.Played), "Bet should be expired if the match is played.");
            Assert.IsTrue(matchVm.Season == $"{match.Season.FirstMatchDay.Year}/{match.Season.EndMatchDate.Year}");
        }

        private Match CreateMatch()
        {
            var club = new Club()
            {
                Id = 1,
                Name = "Arseanal"
            };

            var opponentClub = new Club()
            {
                Id = 2,
                Name = "Spurs"
            };

            var match = new Match()
            {
                MatchDay = DateTime.Today,
                HomeTeam = club,
                AwayTeam = opponentClub,
                HomeGoals = 5,
                AwayGoals = 3,
                MatchStatus = MatchStatus.Played,
                Season = this.season
            };

            return match;
        }

        private void ConfigureMapper()
        {
            Mapper.Initialize(map =>
            {
                map.CreateMap<Match, FixtureMatchViewModel>()
                    .ForMember(vm => vm.HomeTeam, model => model.MapFrom(prop => prop.HomeTeam.Name))
                    .ForMember(vm => vm.AwayTeam, model => model.MapFrom(prop => prop.AwayTeam.Name))
                    .ForMember(vm => vm.AwayTeamId, model => model.MapFrom(prop => prop.AwayTeam.Id))
                    .ForMember(vm => vm.HomeTeamId, model => model.MapFrom(prop => prop.HomeTeam.Id));

                map.CreateMap<Match, MatchDetailsViewModel>()
                    .ForMember(vm => vm.BetsExpired,
                        model => model.MapFrom(prop => prop.MatchStatus == MatchStatus.Played))
                    .ForMember(vm => vm.HomeTeam, model => model.MapFrom(prop => prop.HomeTeam.Name))
                    .ForMember(vm => vm.AwayTeam, model => model.MapFrom(prop => prop.AwayTeam.Name))
                    .ForMember(vm => vm.Stadium, model => model.MapFrom(prop => prop.Stadium.Name))
                    .ForMember(vm => vm.Season,
                        model =>
                            model.MapFrom(prop => prop.Season.FirstMatchDay.Year + "/" + prop.Season.EndMatchDate.Year));

                map.CreateMap<IGrouping<DateTime, Match>, FixturesViewModel>()
                    .ForMember(vm => vm.MatchDay, model => model.MapFrom(prop => prop.Key))
                    .ForMember(vm => vm.Matches, model => model.MapFrom(
                        prop => Mapper.Map<IEnumerable<FixtureMatchViewModel>>(prop.ToList())));
            });
        }
    }
}