﻿namespace UnpremierBetting.Tests.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using UnpremierBetting.Core;
    using UnpremierBetting.Data;
    using UnpremierBetting.Data.Interfaces;
    using UnpremierBetting.Models.EntityModels;
    using UnpremierBetting.Models.Enums;
    using UnpremierBetting.Models.ViewModels.Clubs;
    using UnpremierBetting.Services;
    using UnpremierBetting.Services.Interfaces;
    using Match = UnpremierBetting.Models.EntityModels.Match;

    [TestClass]
    public class ClubsServiceTest
    {
        private IUnpremierBettingData data;
        private IClubsService service;

        [TestInitialize]
        public void Initialize()
        {
            var ctx = new UnpremierBettingContext();
            this.data = new UnpremierBettingData(ctx);

            var pf = new PlayerFactory();
            var cm = new ClubManager(pf);

            this.service = new ClubsService(this.data, cm);

            this.ConfigureMapper();
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void GetClubDetails_EmptyClubsrRepo_ShouldThrowException()
        {
            try
            {
                int clubId = 1;
                this.service.GetClubDetails(clubId);

                Assert.Fail("The service shouldn't return a view model.");
            }
            catch (Exception)
            {
                throw new NullReferenceException();
            }
        }

        [TestMethod]
        public void GetClubDetails_AddClubToRepo_ReturnValidClubData()
        {
            var club = this.CreateClub();
            this.data.Clubs.Add(club);

            var clubVm = this.service.GetClubDetails(club.Id);

            Assert.AreEqual(club.Budget, clubVm.Budget, "The budgets should match.");
            Assert.AreEqual(club.Logo, clubVm.Logo, "The logos should match.");
            Assert.AreEqual(club.Name, clubVm.Name, "The club names should match.");
            Assert.AreEqual(club.Rank, clubVm.Rank, "The ranks should match.");
            Assert.AreEqual(club.Stadium.Name, clubVm.Stadium, "The stadium names should match.");
        }

        [TestMethod]
        public void GetClubDetails_AddClubToRepo_ReturnValidMatches()
        {
            var club = this.CreateClub();
            var clubHomePlayedMatches = club.HomeMatches.Where(m => m.MatchStatus == MatchStatus.Played);
            var clubAwayPlayedMatches = club.AwayMatches.Where(m => m.MatchStatus == MatchStatus.Played);

            var clubHomeUpcomingMatches = club.HomeMatches.Where(m => m.MatchStatus == MatchStatus.Upcoming);
            var clubAwayUpcomingMatches = club.AwayMatches.Where(m => m.MatchStatus == MatchStatus.Upcoming);

            var clubPlayedMatches = new List<Match>();
            clubPlayedMatches.AddRange(clubHomePlayedMatches);
            clubPlayedMatches.AddRange(clubAwayPlayedMatches);

            var clubUpcomingMatches = new List<Match>();
            clubUpcomingMatches.AddRange(clubHomeUpcomingMatches);
            clubUpcomingMatches.AddRange(clubAwayUpcomingMatches);

            this.data.Clubs.Add(club);

            var clubVm = this.service.GetClubDetails(club.Id);

            Assert.IsTrue(
                clubVm.PlayedMatches.All(
                    pm => clubPlayedMatches.Any(
                        m => m.HomeTeam.Name == pm.HomeTeam && 
                        m.AwayTeam.Name == pm.AwayTeam)), "The played matches should match.");

            Assert.IsTrue(
               clubVm.UpcomingMatches.All(
                   pm => clubUpcomingMatches.Any(
                       m => m.HomeTeam.Name == pm.HomeTeam && 
                       m.AwayTeam.Name == pm.AwayTeam)), "The upcoming matches should match.");
        }

        [TestMethod]
        public void GetClubDetails_AddClubToRepo_ReturnValidSquad()
        {
            var club = this.CreateClub();
            this.data.Clubs.Add(club);

            var clubVm = this.service.GetClubDetails(club.Id);

            Assert.IsTrue(clubVm.TopPlayers.All(p => club.Squad.Any(
                sp => sp.FirstName + " " + sp.LastName == p.FullName)), "The names should match.");

            Assert.IsTrue(clubVm.TopPlayers.All(p => club.Squad.Any(
                sp => sp.Overall == p.Overall)), "The overalls should match.");

            Assert.IsTrue(clubVm.TopPlayers.All(p => club.Squad.Any(
                sp => sp.PictureId == p.PictureId)), "The pics should match.");

            Assert.IsTrue(clubVm.TopPlayers.All(p => club.Squad.Any(
                sp => sp.Id == p.Id)), "The ids should match.");

            Assert.IsTrue(clubVm.TopPlayers.All(p => club.Squad.Any(
                sp => sp.Positions.First().Type.ToString() == p.PositionType)), "The position types should match.");
        }

        private Club CreateClub()
        {
            int clubId = 1;

            var stadium = new Stadium()
            {
                Name = "Red Carpet"
            };

            var newPlayer = new Player()
            {
                FirstName = "John",
                LastName = "Rabonata",
                Positions = new List<Position>() { new Position() { Id = "CF", Type = PositionType.Forward } },
                Ability = new Ability() { AttackingLevel = 30, DefensiveLevel = 10, GoalkeepingLevel = 5 },
                PictureId = 1,
            };

            var otherPlayer = new Player()
            {
                FirstName = "Joro",
                LastName = "Vratarcheto",
                Positions = new List<Position>() { new Position() { Id = "GK", Type = PositionType.Goalkeeper } },
                Ability = new Ability() { AttackingLevel = 1, DefensiveLevel = 1, GoalkeepingLevel = 70 },
                PictureId = 2,
            };

            var club = new Club()
            {
                Id = clubId,
                Budget = 1000,
                Rank = 1,
                Logo = "Image.jpg",
                Name = "Arseanal",
                Stadium = stadium
            };

            club.Squad.Add(newPlayer);
            club.Squad.Add(otherPlayer);

            var opponentClub = new Club()
            {
                Name = "Spurs"
            };

            var otherOpponentClub = new Club()
            {
                Name = "Chelsea"
            };

            var homeMatch = new Match()
            {
                HomeTeam = club,
                AwayTeam = opponentClub,
                MatchStatus = MatchStatus.Played
            };

            var awayMatch = new Match()
            {
                HomeTeam = opponentClub,
                AwayTeam = club,
                MatchStatus = MatchStatus.Upcoming
            };

            var otherAwayMatch = new Match()
            {
                HomeTeam = otherOpponentClub,
                AwayTeam = club,
                MatchStatus = MatchStatus.Upcoming
            };

            club.HomeMatches.Add(homeMatch);
            club.AwayMatches.Add(awayMatch);
            club.AwayMatches.Add(otherAwayMatch);

            return club;
        }

        private void ConfigureMapper()
        {
            Mapper.Initialize(map =>
            {
                map.CreateMap<Club, ClubDetailsViewModel>()
                    .ForMember(vm => vm.Stadium, model => model.MapFrom(prop => prop.Stadium.Name));

                map.CreateMap<Player, ClubPlayerViewModel>()
                    .ForMember(vm => vm.FullName, model => model.MapFrom(prop => $"{prop.FirstName} {prop.LastName}"))
                    .ForMember(vm => vm.PositionType, model => model.MapFrom(prop => prop.Positions.First().Type));

                map.CreateMap<Match, ClubMatchViewModel>()
                    .ForMember(vm => vm.HomeTeam, model => model.MapFrom(prop => prop.HomeTeam.Name))
                    .ForMember(vm => vm.AwayTeam, model => model.MapFrom(prop => prop.AwayTeam.Name));
            });
        }
    }
}