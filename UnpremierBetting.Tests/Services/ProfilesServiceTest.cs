﻿namespace UnpremierBetting.Tests.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using UnpremierBetting.Data;
    using UnpremierBetting.Data.Interfaces;
    using UnpremierBetting.Models.EntityModels;
    using UnpremierBetting.Models.EntityModels.Identity;
    using UnpremierBetting.Models.Enums;
    using UnpremierBetting.Models.ViewModels.Profiles;
    using UnpremierBetting.Services;

    [TestClass]
    public class ProfilesServiceTest
    {
        private IUnpremierBettingData data;
        private ProfilesService service;
        private ApplicationUser user;

        [TestInitialize]
        public void Initialize()
        {
            var ctx = new UnpremierBettingContext();
            this.data = new UnpremierBettingData(ctx);
            this.service = new ProfilesService(this.data);

            this.user = new ApplicationUser()
            {
                Id = "Harabeishio"
            };

            this.data.Users.Add(this.user);

            this.ConfigureMapper();
        }

        [TestMethod]
        public void GetUserTickets_AddedUserTickets_ReturnsValidData()
        {
            var userTicket = this.CreateTicket();
            var ticketVm = this.service.GetUserTickets(this.user.Id).First();
            var odds = userTicket.MatchBets.Select(mb => mb.Odds);
            float combinedOdds = odds.Aggregate((b1, b2) => b1 * b2);
            decimal winningAmount = userTicket.Amount * (decimal)combinedOdds;

            Assert.IsTrue(ticketVm.Amount == userTicket.Amount, "The amounts should be equal.");
            Assert.IsTrue(ticketVm.Status == userTicket.Status.ToString(), "The statuses should be equal.");
            Assert.IsTrue(ticketVm.CombinedOdds == combinedOdds, "The odds should be equal.");
            Assert.IsTrue(ticketVm.WinningAmount == winningAmount, "The winning amounts should be equal.");
        }

        [TestMethod]
        public void GetUserTickets_AddedUserTickets_ReturnsValidBets()
        {
            var userTicket = this.CreateTicket();
            var ticketVm = this.service.GetUserTickets(this.user.Id).First();

            Assert.IsTrue(ticketVm.Bets.All(
                b => userTicket.MatchBets.Any(
                    ub => ub.Match.HomeTeam.Name == b.HomeTeam)), "The home team names should match.");

            Assert.IsTrue(ticketVm.Bets.All(
                b => userTicket.MatchBets.Any(
                    ub => ub.Match.AwayTeam.Name == b.AwayTeam)), "The away team names should match.");

            Assert.IsTrue(ticketVm.Bets.All(
                b => userTicket.MatchBets.Any(
                    ub => ub.Match.MatchDay == b.MatchDate)), "The match days should match.");

            Assert.IsTrue(ticketVm.Bets.All(
                b => userTicket.MatchBets.Any(
                    ub => ub.Type.ToString() == b.Type)), "The bet types should match.");

            Assert.IsTrue(ticketVm.Bets.All(
                b => userTicket.MatchBets.Any(
                    ub => ub.Odds == b.Odds)), "The odds should match.");
        }

        [TestMethod]
        public void GetUserTickets_AddedUser_ReturnsValidBalance()
        {
            var userBalance = this.service.GetUserBalance(this.user.Id);

            Assert.AreEqual(this.user.Balance, userBalance, "The balances should be equal.");
        }

        private UserTicket CreateTicket()
        {
            var club = new Club()
            {
                Name = "Assholes"
            };

            var opponentClub = new Club()
            {
                Name = "OtherAssholes"
            };

            var match = new Match()
            {
                HomeTeam = club,
                HomeGoals = 1,
                AwayGoals = 1,
                AwayTeam = opponentClub,
                MatchDay = DateTime.Today
            };

            var matchBet = new MatchBet()
            {
                Type = BetType.Home,
                Match = match,
                Odds = 1.65f
            };

            var userTicket = new UserTicket()
            {
                Amount = 1000,
                Status = TicketStatus.Pending,
            };

            userTicket.MatchBets.Add(matchBet);
            this.user.BetTickets.Add(userTicket);

            return userTicket;
        }

        private void ConfigureMapper()
        {
            Mapper.Initialize(map =>
            {
                map.CreateMap<MatchBet, ProfileTicketBetViewModel>()
               .ForMember(vm => vm.Type, model => model.MapFrom(prop => prop.Type.ToString()))
               .ForMember(vm => vm.HomeTeam, model => model.MapFrom(prop => prop.Match.HomeTeam.Name))
               .ForMember(vm => vm.AwayTeam, model => model.MapFrom(prop => prop.Match.AwayTeam.Name))
               .ForMember(vm => vm.MatchDate, model => model.MapFrom(prop => prop.Match.MatchDay))
               .ForMember(vm => vm.MatchId, model => model.MapFrom(prop => prop.Match.Id));

                map.CreateMap<UserTicket, ProfileTicketViewModel>();
            });
        }
    }
}