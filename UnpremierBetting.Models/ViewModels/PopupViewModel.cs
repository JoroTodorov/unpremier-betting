﻿namespace UnpremierBetting.Models.ViewModels
{
    public class PopupViewModel
    {
        public string Type { get; set; }

        public string BoldMessage { get; set; }

        public string Message { get; set; }
    }
}