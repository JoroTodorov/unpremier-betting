﻿namespace UnpremierBetting.Models.ViewModels.Fixtures
{
    using System;
    using System.Collections.Generic;

    public class FixturesViewModel
    {
        public DateTime MatchDay { get; set; }

        public IEnumerable<FixtureMatchViewModel> Matches { get; set; }
    }
}