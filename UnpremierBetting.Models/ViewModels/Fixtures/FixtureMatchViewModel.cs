﻿namespace UnpremierBetting.Models.ViewModels.Fixtures
{
    using UnpremierBetting.Models.Enums;

    public class FixtureMatchViewModel
    {
        public int Id { get; set; }

        public string HomeTeamId { get; set; }

        public string HomeTeam { get; set; }

        public string AwayTeam { get; set; }

        public string AwayTeamId { get; set; }

        public int HomeGoals { get; set; }

        public int AwayGoals { get; set; }

        public MatchStatus MatchStatus { get; set; }
    }
}