﻿namespace UnpremierBetting.Models.ViewModels.Manage
{
    using System.Collections.Generic;
    using Microsoft.AspNet.Identity;
    using UnpremierBetting.Models.ViewModels.Profiles;

    public class IndexViewModel
    {
        public bool HasPassword { get; set; }

        public decimal Balance { get; set; }

        public IEnumerable<ProfileTicketViewModel> Tickets { get; set; }
    }
}