﻿namespace UnpremierBetting.Models.ViewModels.Players
{
    public class PlayerStatisticViewModel
    {
        public int PlayerId { get; set; }

        public string PlayerName { get; set; }

        public int PictureId { get; set; }

        public int PictureSize { get; set; }

        public int Position { get; set; }

        public int StatCount { get; set; }
    }
}