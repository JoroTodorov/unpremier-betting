﻿using System.Collections.Generic;
using System.ComponentModel;
using UnpremierBetting.Models.Enums;

namespace UnpremierBetting.Models.ViewModels.Players
{
    public class PlayerAbilitiesViewModel
    {
        public string AbilityName { get; set; }

        public int AbilityValue { get; set; }

        public string AbilityColor { get; set; }
    }
}