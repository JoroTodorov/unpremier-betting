﻿using UnpremierBetting.Models.Enums;

namespace UnpremierBetting.Models.ViewModels.Players
{
    using System;
    using System.Collections.Generic;

    public class PlayerDetailsViewModel
    {
        public int Age { get; set; }

        public int PictureId { get; set; }

        public int Overall { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public decimal MarketValue { get; set; }

        public string Club { get; set; }

        public string Nationality { get; set; }

        public PositionType PositionType { get; set; }

        public IEnumerable<string> Positions { get; set; }

        public IEnumerable<PlayerAbilitiesViewModel> MainAbilities { get; set; }

        public IEnumerable<PlayerAbilitiesViewModel> SecondaryAbilities { get; set; }

        public IEnumerable<PlayerMatchViewModel> PlayedMatches { get; set; }
    }
}