﻿namespace UnpremierBetting.Models.ViewModels.Players
{
    public class PlayerMatchViewModel
    {
        public string HomeTeam { get; set; }

        public string AwayTeam { get; set; }

        public int HomeGoals { get; set; }

        public int AwayGoals { get; set; }
    }
}