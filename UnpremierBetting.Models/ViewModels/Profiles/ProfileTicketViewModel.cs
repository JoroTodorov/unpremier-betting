﻿namespace UnpremierBetting.Models.ViewModels.Profiles
{
    using System.Collections.Generic;

    public class ProfileTicketViewModel
    {
        public decimal Amount { get; set; }

        public string Status { get; set; }

        public float CombinedOdds { get; set; }

        public decimal WinningAmount { get; set; }

        public ICollection<ProfileTicketBetViewModel> Bets { get; set; }
    }
}