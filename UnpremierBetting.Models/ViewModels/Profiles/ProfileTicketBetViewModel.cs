﻿namespace UnpremierBetting.Models.ViewModels.Profiles
{
    using System;

    public class ProfileTicketBetViewModel
    {
        public int MatchId { get; set; }

        public string Type { get; set; }

        public float Odds { get; set; }

        public bool IsExpired { get; set; }

        public string HomeTeam { get; set; }

        public string AwayTeam { get; set; }

        public DateTime MatchDate { get; set; }
    }
}