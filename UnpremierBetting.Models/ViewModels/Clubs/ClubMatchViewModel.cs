﻿namespace UnpremierBetting.Models.ViewModels.Clubs
{
    public class ClubMatchViewModel
    {
        public int Id { get; set; }

        public string HomeTeam { get; set; }

        public string AwayTeam { get; set; }

        public int HomeGoals { get; set; }

        public int AwayGoals { get; set; }
    }
}