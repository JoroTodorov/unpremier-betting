﻿namespace UnpremierBetting.Models.ViewModels.Clubs
{
    public class ClubPlayerViewModel
    {
        public int Id { get; set; }

        public string FullName { get; set; }

        public string PositionType { get; set; }

        public int Overall { get; set; }

        public int PictureId { get; set; }

        public string Nationality { get; set; }
    }
}