﻿namespace UnpremierBetting.Models.ViewModels.Clubs
{
    using System.Collections.Generic;

    public class ClubDetailsViewModel
    {
        public ClubDetailsViewModel()
        {
            this.PlayedMatches = new List<ClubMatchViewModel>();
            this.UpcomingMatches = new List<ClubMatchViewModel>();

            this.TopPlayers = new List<ClubPlayerViewModel>();
            this.Bench = new List<ClubPlayerViewModel>();
        }

        public string Name { get; set; }

        public int Rank { get; set; }

        public decimal Budget { get; set; }

        public string Stadium { get; set; }

        public string Logo { get; set; }

        public List<ClubMatchViewModel> PlayedMatches { get; set; }

        public List<ClubMatchViewModel> UpcomingMatches { get; set; }

        public List<ClubPlayerViewModel> TopPlayers { get; set; }

        public List<ClubPlayerViewModel> Bench { get; set; }
    }
}