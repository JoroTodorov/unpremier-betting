﻿namespace UnpremierBetting.Models.ViewModels.Clubs
{
    public class ClubViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool HasSquad { get; set; }
    }
}