﻿namespace UnpremierBetting.Models.ViewModels.Bets
{
    using System.Collections.Generic;
    using System.ComponentModel;

    public class BetTicketViewModel
    {
        [DisplayName("Betting Amount")]
        public decimal BettingAmount { get; set; }

        [DisplayName("Combined Odds")]
        public float CombinedOdds { get; set; }

        public ICollection<BetDetailsViewModel> Bets { get; set; }
    }
}