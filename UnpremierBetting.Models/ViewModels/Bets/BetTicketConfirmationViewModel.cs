﻿namespace UnpremierBetting.Models.ViewModels.Bets
{
    using System.ComponentModel;

    public class BetTicketConfirmationViewModel
    {
        [DisplayName("Betting Amount")]
        public decimal BettingAmount { get; set; }

        [DisplayName("Combined Odds")]
        public float CombinedOdds { get; set; }

        [DisplayName("Winning Amount")]
        public decimal WinningAmount { get; set; }
    }
}