﻿namespace UnpremierBetting.Models.ViewModels.Bets
{
    using System;

    public class BetDetailsViewModel
    {
        public int Id { get; set; }

        public bool IsExpired { get; set; }

        public string Type { get; set; }

        public float Odds { get; set; }

        public string HomeTeam { get; set; }

        public string AwayTeam { get; set; }

        public DateTime MatchDate { get; set; }

        public string  PanelColor { get; set; }

        public int  PanelNumber { get; set; }

        public string ExpiredMessage { get; set; }
    }
}