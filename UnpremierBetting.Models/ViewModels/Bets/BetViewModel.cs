﻿namespace UnpremierBetting.Models.ViewModels.Bets
{
    public class BetViewModel
    {
        public int Id { get; set; }

        public string Type { get; set; }

        public float Odds { get; set; }
    }
}