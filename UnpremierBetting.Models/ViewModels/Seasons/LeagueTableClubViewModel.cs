﻿namespace UnpremierBetting.Models.ViewModels.Seasons
{
    public class LeagueTableClubViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int MatchesPlayed { get; set; }

        public int WinsCount { get; set; }

        public int LossesCount { get; set; }

        public int DrawsCount { get; set; }

        public int Points { get; set; }       

        public int Position { get; set; }  
    }
}