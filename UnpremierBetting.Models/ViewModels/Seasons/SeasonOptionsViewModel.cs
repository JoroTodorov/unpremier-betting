﻿namespace UnpremierBetting.Models.ViewModels.Seasons
{
    using System;
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    public class SeasonOptionsViewModel
    {
        [DisplayName("First Match Date")]
        [DataType(DataType.Date)]
        public DateTime FirstMatchDate { get; set; }
    }
}