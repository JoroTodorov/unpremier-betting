﻿namespace UnpremierBetting.Models.ViewModels.Seasons
{
    public class LeagueViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}