﻿namespace UnpremierBetting.Models.ViewModels.Matches
{
    using System;

    public class MatchDetailsViewModel
    {
        public int Id { get; set; }

        public string HomeTeam { get; set; }

        public string AwayTeam { get; set; }

        public int HomeGoals { get; set; }

        public int AwayGoals { get; set; }

        public DateTime MatchDay { get; set; }

        public string Stadium { get; set; }

        public string Season { get; set; }

        public bool BetsExpired { get; set; }
    }
}