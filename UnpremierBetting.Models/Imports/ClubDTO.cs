﻿namespace UnpremierBetting.Models.Imports
{
    public class ClubDTO
    {
        public string Name { get; set; }

        public int Rank { get; set; }

        public decimal Budget { get; set; }

        public string Logo { get; set; }
    }
}