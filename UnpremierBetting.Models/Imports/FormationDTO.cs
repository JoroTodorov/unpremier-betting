﻿namespace UnpremierBetting.Models.Imports
{
    public class FormationDTO
    {
        public string Type { get; set; }

        public string[] Positions { get; set; }
    }
}