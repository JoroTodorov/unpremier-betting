﻿namespace UnpremierBetting.Models.Imports
{
    public class PositionDTO
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }
    }
}