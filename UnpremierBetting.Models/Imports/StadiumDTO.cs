﻿namespace UnpremierBetting.Models.Imports
{
    public class StadiumDTO
    {
        public string Name { get; set; }

        public string Team { get; set; }

        public int Capacity { get; set; }
    }
}