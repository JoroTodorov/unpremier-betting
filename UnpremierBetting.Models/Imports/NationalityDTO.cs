﻿namespace UnpremierBetting.Models.Imports
{
    public class NationalityDTO
    {
        public string Id { get; set; }

        public string Name { get; set; }
    }
}