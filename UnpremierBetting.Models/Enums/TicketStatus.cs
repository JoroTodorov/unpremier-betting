﻿namespace UnpremierBetting.Models.Enums
{
    public enum TicketStatus
    {
        Won,
        Failed,
        Pending
    }
}