﻿namespace UnpremierBetting.Models.Enums
{
    public enum BetType
    {
        Home,
        Draw,
        Away
    }
}