﻿namespace UnpremierBetting.Models.Enums
{
    public enum MatchStatus
    {
        Played,
        Postponed,
        Canceled,
        Upcoming
    }
}