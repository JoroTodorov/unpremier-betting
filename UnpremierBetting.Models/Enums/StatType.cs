﻿namespace UnpremierBetting.Models.Enums
{
    public enum StatType
    {
        Goal,
        Assist,
        Attendance,
        Pass,
        Tackle,
        Save,
        CleanSheet
    }
}