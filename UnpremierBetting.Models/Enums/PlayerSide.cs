﻿namespace UnpremierBetting.Models.Enums
{
    public enum PlayerSide
    {
        Home,
        Away
    }
}