﻿namespace UnpremierBetting.Models.Enums
{
    public enum PositionType
    {
        Forward,
        Winger,
        Midfielder,
        Defender,
        Goalkeeper
    }
}