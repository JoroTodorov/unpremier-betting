﻿namespace UnpremierBetting.Models.BindingModels
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    public class BetTicketBindingModel
    {
        [Range(5, double.MaxValue, ErrorMessage = "Minimum bet amount is 5")]
        public decimal BettingAmount { get; set; }

        [Range(1, double.MaxValue)]
        public float CombinedOdds { get; set; }
    }
}