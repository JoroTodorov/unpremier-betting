﻿namespace UnpremierBetting.Models.BindingModels
{
    using System.ComponentModel.DataAnnotations;

    public class BetTicketConfirmationBindingModel
    {
        [Range(5, double.MaxValue, ErrorMessage = "Minimum bet amount is 5")]
        public decimal BettingAmount { get; set; }
    }
}