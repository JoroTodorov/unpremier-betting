﻿using System.Collections.Generic;

namespace UnpremierBetting.Models.BindingModels
{
    public class LeagueBindingModel
    {
        public int LeagueId { get; set; }

        public IEnumerable<int> ClubIds { get; set; }
    }
}