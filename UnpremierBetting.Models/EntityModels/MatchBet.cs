﻿namespace UnpremierBetting.Models.EntityModels
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using UnpremierBetting.Models.EntityModels.Identity;
    using UnpremierBetting.Models.Enums;

    public class MatchBet
    {
        public MatchBet()
        {
            this.UserTickets = new HashSet<UserTicket>();
            this.PendingTicketUsers = new HashSet<ApplicationUser>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public BetType Type { get; set; }

        [Required]
        [Range(1, double.MaxValue)]
        public float Odds { get; set; }

        [Required]
        public bool IsExpired { get; set; }

        [Required]
        public virtual Match Match { get; set; }

        [InverseProperty("MatchBets")]
        public virtual ICollection<UserTicket> UserTickets { get; set; }

        public virtual ICollection<ApplicationUser> PendingTicketUsers { get; set; }
    }
}