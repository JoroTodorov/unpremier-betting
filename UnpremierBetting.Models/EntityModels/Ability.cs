﻿using System.ComponentModel;

namespace UnpremierBetting.Models.EntityModels
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using UnpremierBetting.Models.Enums;

    public class Ability
    {
        //Attacking: Speed, Finishing, Passing, Control
        //Defensive: Interceptions, Positioning, Marking, Tackle
        //Goalkeeping: Diving, Positioning, Reflexes

        [Key]
        public int Id { get; set; }

        [Required]
        public virtual Player Player { get; set; }

        [Range(1, 99)]
        public int Speed { get; set; }

        [Range(1, 99)]
        public int Finishing { get; set; }

        [Range(1, 99)]
        public int Passing { get; set; }

        [Range(1, 99)]
        public int Control { get; set; }

        [Range(1, 99)]
        public int Interceptions { get; set; }

        [Range(1, 99)]
        public int Positioning { get; set; }

        [Range(1, 99)]
        public int Marking { get; set; }

        [Range(1, 99)]
        public int Tackle { get; set; }

        [Range(1, 99)]
        [Display(Name = "GK Diving")]
        public int GoalkeeperDiving { get; set; }

        [Range(1, 99)]
        [Display(Name = "GK Reflexes")]
        public int GoalkeeperReflexes { get; set; }

        [Range(1, 99)]
        [Display(Name = "GK Positioning")]
        public int GoalkeeperPositioning { get; set; }
    }
}