﻿namespace UnpremierBetting.Models.EntityModels
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel.DataAnnotations;

    public class Club
    {
        public Club()
        {
            this.Squad =new HashSet<Player>();
            this.Formations =new HashSet<Formation>();
            this.Seasons = new HashSet<SeasonClub>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [Range(1,4)]
        public int Rank { get; set; }

        [Required]      
        public decimal Budget { get; set; }

        public string Logo { get; set; }

        public virtual Stadium Stadium { get; set; }

        public virtual ICollection<League> Leagues { get; set; }    
        
        public virtual ICollection<Player> Squad { get; set; }     
         
        public virtual ICollection<SeasonClub> Seasons { get; set; }

        public virtual ICollection<Formation> Formations { get; set; }
    }
}