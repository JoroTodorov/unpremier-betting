﻿using System.ComponentModel.DataAnnotations;
using UnpremierBetting.Models.Enums;

namespace UnpremierBetting.Models.EntityModels
{
    public class Statistic
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public virtual MatchPlayer Player { get; set; }

        [Range(0, 121)]
        public int Minute { get; set; }

        [Required]
        public bool IsSuccessful { get; set; }

        [Required]
        public StatType StatType { get; set; }
    }
}