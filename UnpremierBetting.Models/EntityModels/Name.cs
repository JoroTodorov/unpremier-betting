﻿namespace UnpremierBetting.Models.EntityModels
{
    using System.ComponentModel.DataAnnotations;

    public class Name
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public string NameIdentifier { get; set; }
    }
}