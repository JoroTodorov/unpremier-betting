﻿namespace UnpremierBetting.Models.EntityModels
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Formation
    {
        public Formation()
        {
            this.Teams = new HashSet<Club>();
            this.PlayerPositions = new HashSet<FormationPosition>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public string Type { get; set; }

        public virtual ICollection<Club> Teams { get; set; }

        public virtual ICollection<FormationPosition> PlayerPositions { get; set; }
    }
}