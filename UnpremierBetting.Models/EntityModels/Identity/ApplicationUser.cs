namespace UnpremierBetting.Models.EntityModels.Identity
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;

    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {
            this.Balance = 1000;
            this.BetTickets = new HashSet<UserTicket>();
            this.PendingTicketMatches = new HashSet<MatchBet>();
        }

        [Required]
        [Range(0, double.MaxValue)]
        public decimal Balance { get; set; }

        public virtual ICollection<UserTicket> BetTickets { get; set; }

        public virtual ICollection<MatchBet> PendingTicketMatches { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
}