﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace UnpremierBetting.Models.EntityModels
{
    public class League
    {
        public League()
        {
            this.Seasons = new HashSet<Season>();
            this.Clubs = new HashSet<Club>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public virtual ICollection<Season> Seasons { get; set; }

        public virtual ICollection<Club> Clubs { get; set; }
    }
}