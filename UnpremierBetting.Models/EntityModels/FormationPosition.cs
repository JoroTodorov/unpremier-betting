﻿namespace UnpremierBetting.Models.EntityModels
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class FormationPosition
    {
        [ForeignKey("Formation")]
        [Column(Order = 1) , Key]
        public int FormationId { get; set; }

        public virtual Formation Formation { get; set; }

        [ForeignKey("Position")]
        [Column(Order = 2), Key]
        public string PositionId { get; set; }

        public virtual Position Position { get; set; }
       
        public int PlayersCount { get; set; }
    }
}