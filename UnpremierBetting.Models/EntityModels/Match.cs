﻿namespace UnpremierBetting.Models.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using UnpremierBetting.Models.Enums;

    public class Match
    {
        public Match()
        {
            this.Players = new HashSet<MatchPlayer>();
            this.MatchBets = new HashSet<MatchBet>();
        }

        [Key]
        public int Id { get; set; }

        [InverseProperty("HomeMatches")]
        public virtual SeasonClub HomeTeam { get; set; }

        [InverseProperty("AwayMatches")]
        public virtual SeasonClub AwayTeam { get; set; }

        [Range(0,20)]
        public int HomeGoals { get; set; }

        [Range(0,20)]
        public int AwayGoals { get; set; }

        [Required]
        public DateTime MatchDay { get; set; }

        [Required]
        public MatchStatus MatchStatus { get; set; }

        public virtual Stadium Stadium { get; set; }

        public virtual ICollection<MatchPlayer> Players { get; set; }

        public virtual ICollection<MatchBet> MatchBets { get; set; }
    }
}