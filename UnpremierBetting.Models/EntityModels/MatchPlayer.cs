﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using UnpremierBetting.Models.Enums;

namespace UnpremierBetting.Models.EntityModels
{
    public class MatchPlayer
    {
        public MatchPlayer()
        {
            this.Statistics = new HashSet<Statistic>();
        }

        [Key]
        public int Id { get; set; }

        public virtual Player Player { get; set; }

        public virtual Match Match { get; set; }

        public PlayerSide Side { get; set; }

        public virtual ICollection<Statistic> Statistics { get; set; }
    }
}