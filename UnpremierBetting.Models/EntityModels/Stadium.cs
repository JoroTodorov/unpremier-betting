﻿namespace UnpremierBetting.Models.EntityModels
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Stadiums")]
    public class Stadium
    {
        public Stadium()
        {
            this.Clubs = new HashSet<Club>();
            this.Matches = new HashSet<Match>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public int Capacity { get; set; }

        public virtual ICollection<Club> Clubs { get; set; }

        public virtual ICollection<Match> Matches { get; set; }
    }
}