﻿using System.Collections.Generic;

namespace UnpremierBetting.Models.EntityModels
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class SeasonClub
    {
        public SeasonClub()
        {
            this.HomeMatches = new HashSet<Match>();
            this.AwayMatches = new HashSet<Match>();
        }

        [Key]
        public int Id { get; set; }

        public virtual Season Season { get; set; }

        public virtual Club Club { get; set; }

        public int Position { get; set; }

        public virtual ICollection<Match> HomeMatches { get; set; }

        public virtual ICollection<Match> AwayMatches { get; set; }
    }
}