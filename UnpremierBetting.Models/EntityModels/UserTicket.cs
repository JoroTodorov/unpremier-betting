﻿namespace UnpremierBetting.Models.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using UnpremierBetting.Models.EntityModels.Identity;
    using UnpremierBetting.Models.Enums;

    public class UserTicket
    {
        public UserTicket()
        {
            this.MatchBets = new HashSet<MatchBet>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        [Range(5, double.MaxValue)]
        public decimal Amount { get; set; }

        [Required]
        public TicketStatus Status { get; set; }

        public virtual ApplicationUser User { get; set; }

        [InverseProperty("UserTickets")]
        public virtual ICollection<MatchBet> MatchBets { get; set; }
    }
}