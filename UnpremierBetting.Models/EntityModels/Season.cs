﻿namespace UnpremierBetting.Models.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Season
    {
        public Season()
        {
            this.Clubs = new HashSet<SeasonClub>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public DateTime FirstMatchDay { get; set; }

        public DateTime EndMatchDate { get; set; }

        [Required]
        public bool IsDeleted { get; set; }

        public virtual League League { get; set; }

        public virtual ICollection<SeasonClub> Clubs { get; set; }
    }
}