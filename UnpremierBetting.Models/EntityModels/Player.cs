﻿namespace UnpremierBetting.Models.EntityModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using UnpremierBetting.Models.Enums;

    public class Player
    {
        public Player()
        {
            this.Matches = new HashSet<MatchPlayer>();
            this.Positions = new HashSet<Position>();
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public DateTime BirthDate { get; set; }

        [NotMapped]
        public int Age => DateTime.Today.Year - this.BirthDate.Year;

        public int PictureId { get; set; }

        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public virtual Ability Ability { get; set; }

        public virtual Club Club { get; set; }

        public virtual Nationality Nationality { get; set; }

        public virtual ICollection<MatchPlayer> Matches { get; set; }

        public virtual ICollection<Position> Positions { get; set; }
    }
}
