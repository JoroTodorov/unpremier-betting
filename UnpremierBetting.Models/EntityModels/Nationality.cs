﻿namespace UnpremierBetting.Models.EntityModels
{
    using System.ComponentModel.DataAnnotations;

    public class Nationality
    {
        [Key, RegularExpression("[A-Z]{2}")]
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }
    }
}