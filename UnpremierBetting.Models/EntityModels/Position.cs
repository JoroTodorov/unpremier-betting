﻿namespace UnpremierBetting.Models.EntityModels
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using UnpremierBetting.Models.Enums;

    public class Position
    {
        public Position()
        {
            this.Players = new HashSet<Player>();
            this.Formations = new HashSet<FormationPosition>();
        }

        [Key, RegularExpression("[A-Z]{2,3}")]
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public PositionType Type { get; set; }

        public virtual ICollection<Player> Players { get; set; }

        public virtual ICollection<FormationPosition> Formations { get; set; }
    }
}