﻿using UnpremierBetting.Utilities.Extensions;

namespace UnpremierBetting.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using UnpremierBetting.Core.Interfaces;
    using UnpremierBetting.Models.EntityModels;
    using UnpremierBetting.Models.Enums;
    using UnpremierBetting.Utilities;

    public class MatchSystem : IMatchSystem
    {
        private readonly IClubManager clubManager;

        public MatchSystem(IClubManager clubManager)
        {
            this.clubManager = clubManager;
        }

        public void CreateMatches(Season season)
        {
            foreach (var homeTeam in season.Clubs)
            {
                var matchDay = season.FirstMatchDay;
                var competitors = season.Clubs.ToList();
                competitors.Remove(homeTeam);

                while (competitors.Any())
                {
                    int teamNum = RandomGenerator.Generate(0, competitors.Count);
                    var awayTeam = competitors[teamNum];

                    int teamsCount = 0;

                    while (this.MatchOverlaps(homeTeam, awayTeam, matchDay))
                    {
                        if (teamsCount >= competitors.Count)
                        {
                            teamsCount = 0;
                            matchDay = matchDay.AddDays(3);
                        }

                        teamNum = (teamNum + 1) % competitors.Count;
                        awayTeam = competitors[teamNum];

                        teamsCount++;
                    }

                    var matchBets = this.CreateMatchBets(homeTeam.Club, awayTeam.Club);

                    var newMatch = new Match()
                    {
                        MatchDay = matchDay,
                        MatchStatus = MatchStatus.Upcoming,
                        Stadium = homeTeam.Club.Stadium,
                        MatchBets = matchBets
                    };

                    homeTeam.HomeMatches.Add(newMatch);
                    awayTeam.AwayMatches.Add(newMatch);

                    competitors.Remove(awayTeam);

                    matchDay = matchDay.AddDays(Constants.DaysBetweenMatches);
                }

                var lastMatchDay = homeTeam.GetMatches().Max(m => m.MatchDay);
                season.EndMatchDate = lastMatchDay > season.EndMatchDate ? lastMatchDay : season.EndMatchDate;
            }
        }

        public void SimulateMatches(DateTime currentDate,
            IEnumerable<FormationPosition> formPositions,
            IEnumerable<SeasonClub> seasonClubs,
            IEnumerable<UserTicket> userTickets)
        {
            var matches = new List<Match>();
            var clubsMatches = seasonClubs.Select(c => c.GetMatches());
            foreach (var clubMatches in clubsMatches)
            {
                var match = clubMatches.FirstOrDefault(cm => cm.MatchDay == currentDate);
                if (match == null || matches.Contains(match))
                {
                    continue;
                }

                matches.Add(match);
            }

            if (!matches.Any())
            {
                return;
            }

            foreach (var match in matches)
            {
                var homePlayers = this.clubManager.ManageTeam(match.HomeTeam.Club, formPositions);
                var awayPlayers = this.clubManager.ManageTeam(match.AwayTeam.Club, formPositions);

                foreach (var homePlayer in homePlayers)
                {
                    homePlayer.Side = PlayerSide.Home;
                    match.Players.Add(homePlayer);
                }

                foreach (var awayPlayer in awayPlayers)
                {
                    awayPlayer.Side = PlayerSide.Away;
                    match.Players.Add(awayPlayer);
                }

                this.SimulateMatch(match);

                this.SetSquadStats(match);
                this.RearrangeClubPositions(seasonClubs);
                this.CheckWinningBets(match, userTickets);
            }
        }

        private void SimulateMatch(Match match)
        {
            int homeAttack, homeMidfield, homeDefence, homeGoalkeeping;
            int awayAttack, awayMidfield, awayDefence, awayGoalkeeping;

            var homePlayers = match.Players.Where(p => p.Side == PlayerSide.Home).Select(p => p.Player);
            this.GetSquadOverall(homePlayers,
                out homeAttack, out homeMidfield, out homeDefence, out homeGoalkeeping);

            var awayPlayers = match.Players.Where(p => p.Side == PlayerSide.Away).Select(p => p.Player);
            this.GetSquadOverall(awayPlayers,
                out awayAttack, out awayMidfield, out awayDefence, out awayGoalkeeping);

            int homeOffence = RandomGenerator.Generate(Constants.MinSquadOverall, homeAttack + homeMidfield);
            int homeDefending = RandomGenerator.Generate(Constants.MinSquadOverall,
                homeDefence + homeGoalkeeping);

            awayAttack = (int)(awayAttack * 0.9d);
            awayDefence = (int)(awayDefence * 0.9d);
            awayMidfield = (int)(awayMidfield * 0.9d);
            awayGoalkeeping = (int)(awayGoalkeeping * 0.9d);

            int awayOffence = RandomGenerator.Generate(Constants.MinSquadOverall, awayAttack + awayMidfield);
            int awayDefending = RandomGenerator.Generate(Constants.MinSquadOverall,
                awayDefence + awayGoalkeeping);

            int homeGoals = (homeOffence - awayDefending) / 100;
            int awayGoals = (awayOffence - homeDefending) / 100;

            match.HomeGoals = homeGoals > 0 ? homeGoals : 0;
            match.AwayGoals = awayGoals > 0 ? awayGoals : 0;

            match.MatchStatus = MatchStatus.Played;
        }

        private bool MatchOverlaps(SeasonClub homeClub, SeasonClub awayClub, DateTime matchDay)
        {
            bool homeOverlaps = homeClub.GetMatches()
                .Any(m => m.MatchDay > matchDay.AddDays(-3) && m.MatchDay < matchDay.AddDays(3));

            bool awayOverlaps = awayClub.GetMatches()
                .Any(m => m.MatchDay > matchDay.AddDays(-3) && m.MatchDay < matchDay.AddDays(3));

            return homeOverlaps || awayOverlaps;
        }

        private ICollection<MatchBet> CreateMatchBets(Club homeTeam, Club awayTeam)
        {
            int homeAttack, homeMidfield, homeDefence, homeGoalkeeping;
            int awayAttack, awayMidfield, awayDefence, awayGoalkeeping;

            this.GetSquadOverall(
                homeTeam.Squad,
                out homeAttack, out homeMidfield, out homeDefence, out homeGoalkeeping);
            this.GetSquadOverall(
                awayTeam.Squad,
                out awayAttack, out awayMidfield, out awayDefence, out awayGoalkeeping);

            int homeOffence = homeAttack + homeMidfield / 2;
            int homeDefending = homeDefence + homeGoalkeeping + homeMidfield / 2;

            int awayOffence = awayAttack + awayMidfield / 2;
            int awayDefending = awayDefence + awayGoalkeeping + awayMidfield / 2;

            float homeBetMultiplier = 1.5f + (awayOffence - homeDefending) / 500f;
            float awayBetMultiplier = 1.5f + (homeOffence - awayDefending) / 500f;
            float drawBetMultiplier = 1 + Math.Abs((homeOffence + homeDefending) - (awayOffence + awayDefending)) / 500f;

            var homeBet = new MatchBet()
            {
                Type = BetType.Home,
                Odds = homeBetMultiplier > 1.05f ? homeBetMultiplier : 1.05f
            };

            var drawBet = new MatchBet()
            {
                Type = BetType.Draw,
                Odds = drawBetMultiplier > 1.05f ? drawBetMultiplier : 1.05f
            };


            var awayBet = new MatchBet()
            {
                Type = BetType.Away,
                Odds = awayBetMultiplier > 1.05f ? awayBetMultiplier : 1.05f
            };

            var matchBets = new List<MatchBet> { homeBet, awayBet, drawBet };

            return matchBets;
        }

        private void CheckWinningBets(Match match, IEnumerable<UserTicket> userTickets)
        {
            BetType winningOdd;

            if (match.HomeGoals > match.AwayGoals)
            {
                winningOdd = BetType.Home;
            }
            else if (match.HomeGoals < match.AwayGoals)
            {
                winningOdd = BetType.Away;
            }
            else
            {
                winningOdd = BetType.Draw;
            }

            var matchBets = match.MatchBets.Where(mb => !mb.Match.IsDeleted());

            foreach (var bet in matchBets)
            {
                bet.IsExpired = true;
                var currentTickets = userTickets.Where(
                    t => t.Status == TicketStatus.Pending
                         && t.MatchBets.Any(b => b.Id == bet.Id));

                if (bet.Type != winningOdd)
                {
                    foreach (var ticket in currentTickets)
                    {
                        ticket.Status = TicketStatus.Failed;
                    }

                    continue;
                }

                foreach (var ticket in currentTickets)
                {
                    if (ticket.MatchBets.Any(b => !b.IsExpired))
                    {
                        continue;
                    }

                    var odds = ticket.MatchBets.Select(m => m.Odds);
                    float multiplier = odds.Aggregate((o1, o2) => o1 * o2);
                    decimal multipliedAmount = ticket.Amount * (decimal)multiplier;

                    ticket.User.Balance += multipliedAmount;
                    ticket.Status = TicketStatus.Won;
                }
            }
        }

        private void RearrangeClubPositions(IEnumerable<SeasonClub> seasonClubs)
        {
            var orderedSeasonClubs = seasonClubs
                .OrderByDescending(c => c.GetPoints())
                .ThenByDescending(c => c.GetWins())
                .ThenByDescending(c => c.GetDraws())
                .ThenBy(c => c.GetMatches().Count(m => m.MatchStatus == MatchStatus.Played))
                .ThenBy(c => c.Club.Name).ToArray();

            for (int position = 0; position < orderedSeasonClubs.Length; position++)
            {
                orderedSeasonClubs[position].Position = position + 1;
            }
        }

        private void SetSquadStats(Match match)
        {
            var homePlayers = match.Players.Where(p => p.Side == PlayerSide.Home);
            var awayPlayers = match.Players.Where(p => p.Side == PlayerSide.Away);

            this.SetDefaultStats(homePlayers, match.AwayGoals);
            this.SetDefaultStats(awayPlayers, match.HomeGoals);

            this.SetAttackerStats(homePlayers, match.HomeGoals);
            this.SetAttackerStats(awayPlayers, match.AwayGoals);

            this.SetDefenderStats(homePlayers);
            this.SetDefenderStats(awayPlayers);
        }

        private void SetAttackerStats(IEnumerable<MatchPlayer> players, int goalsScored)
        {
            var goalscoringPlayers = players
                .OrderByDescending(p => p.Player.Ability.Finishing)
                .ThenByDescending(p => p.Player.CalculateOverall()).Take(3).ToArray();
            var goalscoringSecondaryPlayers = players.Where(p => !goalscoringPlayers.Contains(p)).ToArray();

            var assistingPlayers = players
                .OrderByDescending(p => p.Player.Ability.Passing)
                .ThenByDescending(p => p.Player.Ability.Control).Take(3).ToArray();
            var assistingSecondaryPlayers = players.Where(p => !assistingPlayers.Contains(p)).ToArray();

            int[] goalMinutes = new int[goalsScored];
            for (int i = 0; i < goalsScored; i++)
            {
                int goalMinute;
                do
                {
                    goalMinute = RandomGenerator.Generate(1, 90);
                } while (goalMinutes.Contains(goalMinute));

                goalMinutes[i] = goalMinute;

                var goalStatistic = new Statistic()
                {
                    StatType = StatType.Goal,
                    IsSuccessful = true,
                    Minute = goalMinute
                };

                MatchPlayer goalscorer;
                int secondaryPlayerPossibility = RandomGenerator.Generate(1, 10);
                if (secondaryPlayerPossibility <= 5 && goalscoringPlayers.Length > 0)
                {
                    int playerIndex = RandomGenerator.Generate(0, goalscoringPlayers.Length - 1);
                    goalscorer = goalscoringPlayers[playerIndex];
                }
                else
                {
                    int playerIndex = RandomGenerator.Generate(0, goalscoringSecondaryPlayers.Length - 1);
                    goalscorer = goalscoringSecondaryPlayers[playerIndex];
                }

                goalscorer.Statistics.Add(goalStatistic);

                var assistStatistic = new Statistic()
                {
                    StatType = StatType.Assist,
                    IsSuccessful = true,
                    Minute = goalStatistic.Minute
                };

                MatchPlayer assister;
                var potentialAssisters = assistingPlayers.Where(p => p != goalscorer).ToArray();
                secondaryPlayerPossibility = RandomGenerator.Generate(1, 10);
                if (secondaryPlayerPossibility <= 5 && potentialAssisters.Length > 0)
                {
                    int playerIndex = RandomGenerator.Generate(0, potentialAssisters.Length - 1);
                    assister = potentialAssisters[playerIndex];
                }
                else
                {
                    int playerIndex = RandomGenerator.Generate(0, assistingSecondaryPlayers.Length - 1);
                    assister = assistingSecondaryPlayers[playerIndex];
                }

                assister.Statistics.Add(assistStatistic);
            }
        }

        private void SetDefenderStats(IEnumerable<MatchPlayer> players)
        {
            var defensivePlayers = players
                .Where(p => p.Player.GetPositionType() == PositionType.Defender)
                .OrderByDescending(p => p.Player.CalculateOverall()).ToList();

            if (defensivePlayers.Count == 0)
            {
                var player = players.OrderByDescending(p => p.Player.CalculateOverall()).First();
                defensivePlayers.Add(player);
            }

            var defenderStatistics = new List<Statistic>();
            foreach (var player in defensivePlayers)
            {
                int tackles = RandomGenerator.Generate(0, player.Player.CalculateOverall() * 2) / 10;
                for (int i = 0; i < tackles; i++)
                {
                    var tackleStatistic = new Statistic()
                    {
                        IsSuccessful = Convert.ToBoolean(RandomGenerator.Generate(0, 1)),
                        Minute = RandomGenerator.Generate(1, 90),
                        StatType = StatType.Tackle
                    };

                    player.Statistics.Add(tackleStatistic);
                    defenderStatistics.Add(tackleStatistic);
                }
            }
        }

        private void SetDefaultStats(IEnumerable<MatchPlayer> players, int goalsConceded)
        {
            var defaultStatistics = new List<Statistic>();

            foreach (var player in players)
            {
                var attendanceStatistic = new Statistic()
                {
                    StatType = StatType.Attendance,
                    IsSuccessful = true
                };
                var cleanSheetStatistic = new Statistic()
                {
                    StatType = StatType.CleanSheet,
                    IsSuccessful = goalsConceded == 0
                };

                player.Statistics.Add(attendanceStatistic);
                player.Statistics.Add(cleanSheetStatistic);

                defaultStatistics.Add(attendanceStatistic);
                defaultStatistics.Add(cleanSheetStatistic);
            }
        }

        private void GetSquadOverall(IEnumerable<Player> squad,
            out int attack, out int midfield, out int defence, out int goalkeeping)
        {
            attack = squad
                         .Where(p => p.GetPositionType() == PositionType.Forward)
                         .Sum(p => p.CalculateOverall() * 2) +
                     squad
                         .Where(p => p.GetPositionType() == PositionType.Winger)
                         .Sum(p => p.CalculateOverall());

            midfield = squad
                .Where(p => p.GetPositionType() == PositionType.Midfielder)
                .Sum(p => p.CalculateOverall());

            defence = squad
                .Where(p => p.GetPositionType() == PositionType.Defender)
                .Sum(p => p.CalculateOverall());

            goalkeeping = squad
                              .FirstOrDefault(p => p.GetPositionType() == PositionType.Goalkeeper)
                              ?.CalculateOverall() * 2 ?? 50;
        }
    }
}