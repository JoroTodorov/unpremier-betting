﻿namespace UnpremierBetting.Core.Interfaces
{
    using System.Collections.Generic;
    using UnpremierBetting.Models.EntityModels;

    public interface IClubManager
    {
        void FillClubSquads(
            IEnumerable<Name> names,
            IEnumerable<Nationality> nationalities,
            IEnumerable<Position> positions,
            IEnumerable<Club> clubs);

        IEnumerable<MatchPlayer> ManageTeam(Club club, IEnumerable<FormationPosition> formPositions);
    }
}