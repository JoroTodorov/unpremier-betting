﻿namespace UnpremierBetting.Core.Interfaces
{
    using System.Collections.Generic;
    using UnpremierBetting.Models.EntityModels;

    public interface IPlayerFactory
    {
        Player CreatePlayer(
            IEnumerable<Name> names,
            IEnumerable<Nationality> nationalities,
            IEnumerable<Position> playerPositions,
            int clubRank);
    }
}