﻿namespace UnpremierBetting.Core.Interfaces
{
    using System;
    using System.Collections.Generic;
    using UnpremierBetting.Models.EntityModels;

    public interface IMatchSystem
    {
        void CreateMatches(Season season);

        void SimulateMatches(DateTime currentDate,
            IEnumerable<FormationPosition> formPositions,
            IEnumerable<SeasonClub> seasonClubs,
            IEnumerable<UserTicket> userTickets);
    }
}