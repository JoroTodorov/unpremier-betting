﻿using UnpremierBetting.Utilities.Extensions;

namespace UnpremierBetting.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UnpremierBetting.Core.Interfaces;
    using UnpremierBetting.Models.EntityModels;
    using UnpremierBetting.Models.Enums;
    using UnpremierBetting.Utilities;

    public class PlayerFactory : IPlayerFactory
    {
        public Player CreatePlayer(
            IEnumerable<Name> names,
            IEnumerable<Nationality> nationalities,
            IEnumerable<Position> playerPositions,
            int clubRank)
        {
            string firstName = this.GetName(names);
            string lastName = this.GetName(names);

            var nationality = this.GetNationality(nationalities);
            var date = this.GetBirthDate();

            int pictureId = RandomGenerator.Generate(0, Constants.PlayerPicturesCount);

            var newPlayer = new Player()
            {
                FirstName = firstName,
                LastName = lastName,
                Nationality = nationality,
                BirthDate = date,
                PictureId = pictureId
            };

            foreach (var position in playerPositions)
            {
                newPlayer.Positions.Add(position);
            }

            this.GetAbility(clubRank, newPlayer);

            return newPlayer;
        }

        private string GetName(IEnumerable<Name> names)
        {
            int id = RandomGenerator.Generate(0, names.Count() - 1);

            return names.ToArray()[id].NameIdentifier;
        }

        private Nationality GetNationality(IEnumerable<Nationality> nationalities)
        {
            bool isEnglish = Convert.ToBoolean(RandomGenerator.Generate(0, 3));
            if (isEnglish)
            {
                return nationalities.First(n => n.Id == Constants.UnitedKingdomIsoCode);
            }

            int id = RandomGenerator.Generate(0, nationalities.Count() - 1);

            return nationalities.ToArray()[id];
        }

        private DateTime GetBirthDate()
        {
            var minDate = DateTime.Today.AddYears(-Constants.MinProfessionalAge);
            var maxDate = DateTime.Today.AddYears(-Constants.MaxProfessionalAge);

            int range = (minDate - maxDate).Days;
            var date = maxDate.AddDays(RandomGenerator.Generate(0, range));

            return date;
        }

        private void GetAbility(int clubRank, Player player)
        {
            int levelSubstract = RandomGenerator.Generate(clubRank, 10) * clubRank;

            if (player.Age <= 21 || player.Age >= 28)
            {
                levelSubstract = RandomGenerator.Generate(5, 10) * clubRank;
            }

            Ability ability = new Ability();

            switch (player.GetPositionType())
            {
                case PositionType.Goalkeeper:
                    ability.Finishing = RandomGenerator.Generate(10, 20);
                    ability.Speed = RandomGenerator.Generate(20, 50);
                    ability.Passing = RandomGenerator.Generate(10, 40);
                    ability.Control = RandomGenerator.Generate(10, 20);
                    ability.Interceptions = RandomGenerator.Generate(10, 20);
                    ability.Positioning = RandomGenerator.Generate(10, 20);
                    ability.Tackle = RandomGenerator.Generate(10, 20);
                    ability.Marking = RandomGenerator.Generate(10, 20);
                    ability.GoalkeeperReflexes = RandomGenerator.Generate(70, 100) - levelSubstract;
                    ability.GoalkeeperDiving = RandomGenerator.Generate(70, 100) - levelSubstract;
                    ability.GoalkeeperPositioning = RandomGenerator.Generate(70, 100) - levelSubstract;
                    break;
                case PositionType.Defender:
                    ability.Finishing = RandomGenerator.Generate(10, 30);
                    ability.Speed = RandomGenerator.Generate(30, 60);
                    ability.Passing = RandomGenerator.Generate(20, 50);
                    ability.Control = RandomGenerator.Generate(20, 50);
                    ability.Interceptions = RandomGenerator.Generate(60, 100) - levelSubstract;
                    ability.Positioning = RandomGenerator.Generate(60, 100) - levelSubstract;
                    ability.Tackle = RandomGenerator.Generate(70, 100) - levelSubstract;
                    ability.Marking = RandomGenerator.Generate(70, 100) - levelSubstract;
                    ability.GoalkeeperReflexes = RandomGenerator.Generate(10, 20);
                    ability.GoalkeeperDiving = RandomGenerator.Generate(10, 20);
                    ability.GoalkeeperPositioning = RandomGenerator.Generate(10, 20);
                    break;
                case PositionType.Midfielder:
                    ability.Finishing = RandomGenerator.Generate(20, 60);
                    ability.Speed = RandomGenerator.Generate(40, 70);
                    ability.Passing = RandomGenerator.Generate(70, 100) - levelSubstract;
                    ability.Control = RandomGenerator.Generate(70, 100) - levelSubstract;
                    ability.Interceptions = RandomGenerator.Generate(50, 90) - levelSubstract;
                    ability.Positioning = RandomGenerator.Generate(20, 60);
                    ability.Tackle = RandomGenerator.Generate(50, 90) - levelSubstract;
                    ability.Marking = RandomGenerator.Generate(20, 60);
                    ability.GoalkeeperReflexes = RandomGenerator.Generate(10, 20);
                    ability.GoalkeeperDiving = RandomGenerator.Generate(10, 20);
                    ability.GoalkeeperPositioning = RandomGenerator.Generate(10, 20);
                    break;
                case PositionType.Winger:
                    ability.Finishing = RandomGenerator.Generate(60, 90) - levelSubstract;
                    ability.Speed = RandomGenerator.Generate(80, 100) - levelSubstract;
                    ability.Passing = RandomGenerator.Generate(50, 80) - levelSubstract;
                    ability.Control = RandomGenerator.Generate(70, 100) - levelSubstract;
                    ability.Interceptions = RandomGenerator.Generate(10, 40);
                    ability.Positioning = RandomGenerator.Generate(10, 40);
                    ability.Tackle = RandomGenerator.Generate(10, 20);
                    ability.Marking = RandomGenerator.Generate(10, 20);
                    ability.GoalkeeperReflexes = RandomGenerator.Generate(10, 20);
                    ability.GoalkeeperDiving = RandomGenerator.Generate(10, 20);
                    ability.GoalkeeperPositioning = RandomGenerator.Generate(1, 20);
                    break;

                case PositionType.Forward:
                    ability.Finishing = RandomGenerator.Generate(70, 100) - levelSubstract;
                    ability.Speed = RandomGenerator.Generate(70, 100) - levelSubstract;
                    ability.Passing = RandomGenerator.Generate(30, 60);
                    ability.Control = RandomGenerator.Generate(70, 100) - levelSubstract;
                    ability.Interceptions = RandomGenerator.Generate(10, 30);
                    ability.Positioning = RandomGenerator.Generate(10, 30);
                    ability.Tackle = RandomGenerator.Generate(10, 30);
                    ability.Marking = RandomGenerator.Generate(10, 20);
                    ability.GoalkeeperReflexes = RandomGenerator.Generate(10, 20);
                    ability.GoalkeeperDiving = RandomGenerator.Generate(10, 20);
                    ability.GoalkeeperPositioning = RandomGenerator.Generate(10, 20);
                    break;

                default:
                    throw new ArgumentException("Position dosen't exist.");
            }

            player.Ability = ability;
        }
    }
}