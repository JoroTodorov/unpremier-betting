﻿using UnpremierBetting.Models.Enums;
using UnpremierBetting.Utilities.Extensions;

namespace UnpremierBetting.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UnpremierBetting.Core.Interfaces;
    using UnpremierBetting.Models.EntityModels;
    using UnpremierBetting.Utilities;

    public class ClubManager : IClubManager
    {
        public ClubManager(IPlayerFactory playerFactory)
        {
            this.PlayerFactory = playerFactory;
        }

        private IPlayerFactory PlayerFactory { get; }

        public void FillClubSquads(
            IEnumerable<Name> names,
            IEnumerable<Nationality> nationalities,
            IEnumerable<Position> positions,
            IEnumerable<Club> clubs)
        {
            foreach (var club in clubs)
            {
                if (club.Squad.Any())
                {
                    throw new ArgumentException("Club has a full squad already.");
                }
                int formationIndex = 0;
                int posIndex = 0;
                var formationsPositions = club.Formations.Select(f => f.PlayerPositions).ToArray();

                //Creates players suitable for the club's formations
                int playersCount = 30 - club.Rank * 4;
                for (int i = 0; i < playersCount;)
                {
                    if (posIndex % formationsPositions[formationIndex].Count == 0)
                    {
                        formationIndex++;
                        formationIndex %= formationsPositions.Length;
                        posIndex = 0;
                    }

                    var formationPosition = formationsPositions[formationIndex].ToArray()[posIndex];
                    var playerPositions = positions.Where(p => p.Type == formationPosition.Position.Type);
                    for (int count = 1; count <= formationPosition.PlayersCount; count++)
                    {
                        var newPlayer = this.PlayerFactory.CreatePlayer(
                            names, nationalities, playerPositions, club.Rank);

                        club.Squad.Add(newPlayer);

                        i++;
                    }

                    posIndex++;
                }
            }
        }

        public IEnumerable<MatchPlayer> ManageTeam(Club club, IEnumerable<FormationPosition> formPositions)
        {
            var formations = club.Formations.ToArray();
            var formationId = formations[RandomGenerator.Generate(0, formations.Length)].Id;
            var formationPositions = formPositions.Where(
                f => f.FormationId == formationId);

            var players = new List<MatchPlayer>();
            var squad = club.Squad.OrderByDescending(p => p.CalculateOverall()).ToList();

            foreach (var formationPosition in formationPositions)
            {
                for (int i = 0; i < formationPosition.PlayersCount; i++)
                {
                    var player = squad.FirstOrDefault(p => p.GetPositionType() == formationPosition.Position.Type) ??
                                 squad.First(p => p.GetPositionType() != PositionType.Goalkeeper);

                    var matchPlayer = new MatchPlayer() { Player = player };
                    players.Add(matchPlayer);

                    squad.Remove(player);
                }
            }

            return players;
        }

        public IEnumerable<Position> GetPositions(IEnumerable<Position> positions, PositionType positionType)
        {
            var playerPositions = positions.Where(p => p.Type == positionType);

            return playerPositions;
        }
    }
}