﻿namespace UnpremierBetting.Utilities
{
    using System.Collections.Generic;
    using System.Linq;
    using UnpremierBetting.Models.ViewModels.Fixtures;

    public class Paginator
    {
        private static int itemsPerPage;

        public static int ItemsPerPage
        {
            get => itemsPerPage <= 0 ? 10 : itemsPerPage;
            set => itemsPerPage = value;
        }

        public static int GetPagesCount(int itemsCount)
        {
            return itemsCount / ItemsPerPage + (itemsCount % ItemsPerPage > 0 ? 1 : 0);
        }

        public static IEnumerable<Т> GetPageItems<Т>(IEnumerable<Т> items, int page = 1)
        {
            int skipNum = (page - 1) * ItemsPerPage;
            var pageItems = items.Skip(skipNum).Take(ItemsPerPage);

            return pageItems;
        }

        public static int GetActiveFixturesPage(IEnumerable<FixturesViewModel> fixtures)
        {
            int fixturesCount = 1;
            foreach (var fixture in fixtures)
            {
                if (fixture.MatchDay >= Constants.ServerTime)
                {
                    break;
                }

                fixturesCount++;
            }

            int page = fixturesCount / ItemsPerPage + (fixturesCount % ItemsPerPage > 0 ? 1 : 0);

            return page;
        }
    }
}