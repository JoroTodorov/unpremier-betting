﻿namespace UnpremierBetting.Utilities.Attributes
{
    using System.Web.Mvc;

    public class AdminAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (!filterContext.HttpContext.User.IsInRole("Administrator"))
            {
                filterContext.Result = new RedirectResult("/"); 
            }

            base.OnAuthorization(filterContext);
        }
    }
}