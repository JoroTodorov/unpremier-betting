﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using UnpremierBetting.Models.BindingModels;
using UnpremierBetting.Models.EntityModels;
using UnpremierBetting.Models.Enums;
using UnpremierBetting.Models.Imports;
using UnpremierBetting.Models.ViewModels.Bets;
using UnpremierBetting.Models.ViewModels.Clubs;
using UnpremierBetting.Models.ViewModels.Fixtures;
using UnpremierBetting.Models.ViewModels.Matches;
using UnpremierBetting.Models.ViewModels.Players;
using UnpremierBetting.Models.ViewModels.Profiles;
using UnpremierBetting.Models.ViewModels.Seasons;
using UnpremierBetting.Utilities.Extensions;

namespace UnpremierBetting.Utilities
{
    public static class MapperConfig
    {
        public static void Initialize()
        {
            Mapper.Initialize(map =>
            {
                MapDtos(map);
                MapMatches(map);
                MapPlayers(map);
                MapMatchBets(map);

                map.CreateMap<Club, ClubDetailsViewModel>()
                    .ForMember(vm => vm.Stadium, model => model.MapFrom(prop => prop.Stadium.Name));

                map.CreateMap<Club, ClubViewModel>()
                    .ForMember(vm => vm.HasSquad, model => model.MapFrom(prop => prop.Squad.Any()));

                map.CreateMap<Season, SelectListItem>()
                    .ForMember(item => item.Text, model => model.MapFrom(
                        prop => prop.FirstMatchDay.Year + "/" + prop.EndMatchDate.Year))
                    .ForMember(item => item.Value, model => model.MapFrom(
                        prop => prop.FirstMatchDay.Year));

                map.CreateMap<League, SelectListItem>()
                    .ForMember(item => item.Text, model => model.MapFrom(prop => prop.Name))
                    .ForMember(item => item.Value, model => model.MapFrom(prop => prop.Id));

                map.CreateMap<SeasonClub, LeagueTableClubViewModel>()
                    .ForMember(vm => vm.Id, model => model.MapFrom(prop => prop.Club.Id))
                    .ForMember(vm => vm.Name, model => model.MapFrom(prop => prop.Club.Name))
                    .ForMember(vm => vm.WinsCount, model => model.MapFrom(prop => prop.GetWins()))
                    .ForMember(vm => vm.DrawsCount, model => model.MapFrom(prop => prop.GetDraws()))
                    .ForMember(vm => vm.LossesCount, model => model.MapFrom(prop => prop.GetLosses()))
                    .ForMember(vm => vm.MatchesPlayed, model => model.MapFrom(
                        prop => prop.GetMatches().Count(m => m.MatchStatus == MatchStatus.Played)))
                    .ForMember(vm => vm.Points, model => model.MapFrom(prop => prop.GetPoints()));

                map.CreateMap<UserTicket, ProfileTicketViewModel>()
                    .ForMember(vm => vm.Bets, model => model.MapFrom(
                        prop => Mapper.Map<IEnumerable<ProfileTicketBetViewModel>>(prop.MatchBets)));

                map.CreateMap<BetTicketBindingModel, BetTicketConfirmationViewModel>()
                    .ForMember(vm => vm.WinningAmount,
                        model => model.MapFrom(prop => (decimal) prop.CombinedOdds * prop.BettingAmount));
            });
        }

        private static void MapDtos(IMapperConfigurationExpression map)
        {
            map.CreateMap<NameDTO, Name>()
                .ForMember(nameModel => nameModel.NameIdentifier, nameDto => nameDto.MapFrom(p => p.Name));
            map.CreateMap<NationalityDTO, Nationality>();
            map.CreateMap<PositionDTO, Position>()
                .ForMember(posModel => posModel.Type,
                    posDto => posDto.MapFrom(p => (PositionType)Enum.Parse(typeof(PositionType), p.Type)));
            map.CreateMap<ClubDTO, Club>();
            map.CreateMap<StadiumDTO, Stadium>();
            map.CreateMap<FormationDTO, Formation>();
        }

        private static void MapMatches(IMapperConfigurationExpression map)
        {
            map.CreateMap<Match, ClubMatchViewModel>()
                .ForMember(vm => vm.HomeTeam, model => model.MapFrom(prop => prop.HomeTeam.Club.Name))
                .ForMember(vm => vm.AwayTeam, model => model.MapFrom(prop => prop.AwayTeam.Club.Name));

            map.CreateMap<Match, PlayerMatchViewModel>()
                .ForMember(vm => vm.HomeTeam, model => model.MapFrom(prop => prop.HomeTeam.Club.Name))
                .ForMember(vm => vm.AwayTeam, model => model.MapFrom(prop => prop.AwayTeam.Club.Name));

            map.CreateMap<Match, FixtureMatchViewModel>()
                .ForMember(vm => vm.HomeTeam, model => model.MapFrom(prop => prop.HomeTeam.Club.Name))
                .ForMember(vm => vm.AwayTeam, model => model.MapFrom(prop => prop.AwayTeam.Club.Name))
                .ForMember(vm => vm.AwayTeamId, model => model.MapFrom(prop => prop.AwayTeam.Club.Id))
                .ForMember(vm => vm.HomeTeamId, model => model.MapFrom(prop => prop.HomeTeam.Club.Id));

            map.CreateMap<Match, MatchDetailsViewModel>()
                .ForMember(vm => vm.BetsExpired,
                    model => model.MapFrom(prop => prop.MatchStatus == MatchStatus.Played))
                .ForMember(vm => vm.HomeTeam, model => model.MapFrom(prop => prop.HomeTeam.Club.Name))
                .ForMember(vm => vm.AwayTeam, model => model.MapFrom(prop => prop.AwayTeam.Club.Name))
                .ForMember(vm => vm.Stadium, model => model.MapFrom(prop => prop.Stadium.Name))
                .ForMember(vm => vm.Season,
                    model =>
                        model.MapFrom(prop => prop.HomeTeam.Season.FirstMatchDay.Year + "/" +
                                              prop.HomeTeam.Season.EndMatchDate.Year));
        }

        private static void MapPlayers(IMapperConfigurationExpression map)
        {
            map.CreateMap<Player, ClubPlayerViewModel>()
                .ForMember(vm => vm.FullName, model => model.MapFrom(prop => $"{prop.FirstName} {prop.LastName}"))
                .ForMember(vm => vm.PositionType, model => model.MapFrom(prop => prop.GetPositionType()))
                .ForMember(vm => vm.Nationality, model => model.MapFrom(prop => prop.Nationality.Name))
                .ForMember(vm => vm.Overall, model => model.MapFrom(prop => prop.CalculateOverall()));

            map.CreateMap<Player, PlayerDetailsViewModel>()
                .ForMember(vm => vm.Club, model => model.MapFrom(prop => prop.Club.Name))
                .ForMember(vm => vm.Nationality, model => model.MapFrom(prop => prop.Nationality.Name))
                .ForMember(vm => vm.Positions, model => model.MapFrom(prop => prop.Positions.Select(p => p.Id)))
                .ForMember(vm => vm.Overall, model => model.MapFrom(prop => prop.CalculateOverall()))
                .ForMember(vm => vm.PositionType, model => model.MapFrom(prop => prop.GetPositionType()))
                .ForMember(vm => vm.MarketValue, model => model.MapFrom(prop => prop.GetMarketValue()));
        }

        private static void MapMatchBets(IMapperConfigurationExpression map)
        {
            map.CreateMap<MatchBet, ProfileTicketBetViewModel>()
                .ForMember(vm => vm.Type, model => model.MapFrom(prop => prop.Type.ToString()))
                .ForMember(vm => vm.HomeTeam, model => model.MapFrom(prop => prop.Match.HomeTeam.Club.Name))
                .ForMember(vm => vm.AwayTeam, model => model.MapFrom(prop => prop.Match.AwayTeam.Club.Name))
                .ForMember(vm => vm.MatchDate, model => model.MapFrom(prop => prop.Match.MatchDay))
                .ForMember(vm => vm.MatchId, model => model.MapFrom(prop => prop.Match.Id));

            map.CreateMap<MatchBet, BetViewModel>()
                .ForMember(vm => vm.Type, model => model.MapFrom(prop => prop.Type.ToString()));

            map.CreateMap<MatchBet, BetDetailsViewModel>()
                .ForMember(vm => vm.Type, model => model.MapFrom(prop => prop.Type.ToString()))
                .ForMember(vm => vm.HomeTeam, model => model.MapFrom(prop => prop.Match.HomeTeam.Club.Name))
                .ForMember(vm => vm.AwayTeam, model => model.MapFrom(prop => prop.Match.AwayTeam.Club.Name))
                .ForMember(vm => vm.MatchDate, model => model.MapFrom(prop => prop.Match.MatchDay));
        }
    }
}
