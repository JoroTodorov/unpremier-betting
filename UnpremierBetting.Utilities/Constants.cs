﻿namespace UnpremierBetting.Utilities
{
    using System;

    public class Constants
    {
        public static DateTime ServerTime;

        public const int MinSquadOverall = 100;

        public const int PlayerPicturesCount = 432;

        public const int MinProfessionalAge = 18;

        public const int MaxProfessionalAge = 36;

        public const long MarketValueDivider = 1_000_000_000_000;

        public const int DaysBetweenMatches = 14;

        public const int MinNamesCount = 1000;

        public const int MinNationalitiesCount = 20;

        public const int MinPositionsCount = 8;

        public const int MinClubsCount = 20;

        public const int MinStatTypesCount = 3;

        public const int MinFormationsCount = 5;

        public const string NamesImportPath = "/Imports/names.json";

        public const string NationalitiesImportPath = "/Imports/countries.json";

        public const string PositionsImportPath = "/Imports/positions.json";

        public const string ClubsImportPath = "/Imports/teams.json";

        public const string StadiumsImportPath = "/Imports/stadiums.json";

        public const string StatTypesImportPath = "/Imports/statTypes.json";

        public const string FormationsImportPath = "/Imports/formations.json";

        public const string DropboxAccessToken = "TEeVC2K__HAAAAAAAAAADCCAnr8pxjR7m3Ud7cmTLuZtwpN2Y9dnMK84Qo7tSPaB";

        public const string UnitedKingdomIsoCode = "GB";
    }
}