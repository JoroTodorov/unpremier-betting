﻿namespace UnpremierBetting.Utilities.Exceptions
{
    using System;

    public class ImportException : Exception
    {
        private const string ImportErrorMessage = 
            "The system demands the valid {0} count to be {1} or more.";

        public ImportException(string modelsType, int modelsCount)
        {
            this.ModelsType = modelsType;
            this.ModelsCount = modelsCount;
        }

        public string ModelsType { get; }

        public int ModelsCount { get; }

        public override string Message => 
            string.Format(ImportErrorMessage, this.ModelsType, this.ModelsCount);
    }
}