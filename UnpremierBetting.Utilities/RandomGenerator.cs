﻿namespace UnpremierBetting.Utilities
{
    using System;

    public static class RandomGenerator
    {
        private static readonly Random random = new Random();

        public static int Generate(int min, int max)
        {
            return random.Next(min, max);
        }
    }
}