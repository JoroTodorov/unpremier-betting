using System.Collections.Generic;
using System.Linq;
using UnpremierBetting.Models.EntityModels;
using UnpremierBetting.Models.Enums;

namespace UnpremierBetting.Utilities.Extensions
{
    public static class ClubExtensions
    {
        public static IEnumerable<Match> GetMatches(this SeasonClub club)
        {
            var allMatches = new List<Match>(club.HomeMatches);
            allMatches.AddRange(club.AwayMatches);

            return allMatches;
        }

        public static int GetWins(this SeasonClub club)
        {
            return club.HomeMatches.Count(m => m.MatchStatus == MatchStatus.Played && m.HomeGoals > m.AwayGoals) +
                   club.AwayMatches.Count(m => m.MatchStatus == MatchStatus.Played && m.AwayGoals > m.HomeGoals);
        }

        public static int GetDraws(this SeasonClub club)
        {
            return club.GetMatches().Count(m => m.MatchStatus == MatchStatus.Played && m.HomeGoals == m.AwayGoals);
        }

        public static int GetLosses(this SeasonClub club)
        {
            return club.HomeMatches.Count(m => m.MatchStatus == MatchStatus.Played && m.HomeGoals < m.AwayGoals) +
                   club.AwayMatches.Count(m => m.MatchStatus == MatchStatus.Played && m.AwayGoals < m.HomeGoals);
        }

        public static int GetPoints(this SeasonClub club)
        {
            return club.GetWins() * 3 + club.GetDraws();
        }

        public static Dictionary<Player, List<Statistic>> GetStatistics(this SeasonClub club, StatType statType)
        {
            var clubStatistics = new Dictionary<Player, List<Statistic>>();
            var matches = club.GetMatches().Where(m => m.MatchStatus == MatchStatus.Played);

            foreach (var match in matches)
            {
                var clubSide = match.HomeTeam == club ? PlayerSide.Home : PlayerSide.Away;
                var matchPlayers = match.Players.Where(p => p.Side == clubSide);
                foreach (var matchPlayer in matchPlayers)
                {
                    if (!clubStatistics.ContainsKey(matchPlayer.Player))
                    {
                        clubStatistics.Add(matchPlayer.Player, new List<Statistic>());
                    }
                    clubStatistics[matchPlayer.Player].AddRange(
                        matchPlayer.Statistics.Where(s => s.StatType == statType));
                }
            }

            return clubStatistics;
        }
    }
}