﻿using System;
using System.Linq;
using UnpremierBetting.Models.EntityModels;
using UnpremierBetting.Models.Enums;

namespace UnpremierBetting.Utilities.Extensions
{
    public static class PlayerExtensions
    {
        public static int CalculateOverall(this Player player)
        {
            var position = player.Positions.First().Type;

            int overall;

            switch (position)
            {
                case PositionType.Forward:
                    overall = (player.Ability.Finishing * 3
                               + player.Ability.Speed
                               + player.Ability.Control) / 5;
                    break;
                case PositionType.Winger:
                    overall = (player.Ability.Speed * 2
                               + player.Ability.Control * 2
                               + player.Ability.Finishing
                               + player.Ability.Passing) / 6;
                    break;
                case PositionType.Defender:
                    overall = (player.Ability.Marking * 2
                               + player.Ability.Tackle * 2
                               + player.Ability.Interceptions
                               + player.Ability.Positioning) / 6;
                    break;
                case PositionType.Goalkeeper:
                    overall = (player.Ability.GoalkeeperDiving
                               + player.Ability.GoalkeeperPositioning
                               + player.Ability.GoalkeeperReflexes) / 3;
                    break;
                case PositionType.Midfielder:
                    overall = (player.Ability.Control * 2
                               + player.Ability.Passing * 2
                               + player.Ability.Interceptions / 2
                               + player.Ability.Tackle / 2) / 5;
                    break;
                default:
                    overall = 0;
                    break;
            }

            return overall;
        }

        public static PositionType GetPositionType(this Player player)
        {
            return player.Positions.First().Type;
        }

        public static decimal GetMarketValue(this Player player)
        {
            decimal overallValue = (decimal)Math.Pow(player.CalculateOverall(), 10);
            decimal ageMultiplier = Constants.MaxProfessionalAge / (decimal)player.Age;
            decimal marketValue = overallValue * ageMultiplier / Constants.MarketValueDivider;

            decimal nationalityBonus = player.Nationality.Id == Constants.UnitedKingdomIsoCode ? 1.1m : 1;

            var posType = player.GetPositionType();
            decimal posBonus = posType == PositionType.Forward || posType == PositionType.Winger
                ? 1.2m
                : posType == PositionType.Goalkeeper
                    ? 1.1m
                    : posType == PositionType.Midfielder
                        ? 1m
                        : 0.9m;

            return marketValue * nationalityBonus * posBonus;
        }

        public static string GetFullName(this Player player)
        {
            return $"{player.FirstName} {player.LastName}";
        }
    }
}