﻿using System;
using System.Linq.Expressions;
using UnpremierBetting.Models.EntityModels;

namespace UnpremierBetting.Utilities.Extensions
{
    public static class MatchExtensions
    {
        public static bool IsDeleted(this Match match)
        {
            return match.HomeTeam.Season.IsDeleted && match.AwayTeam.Season.IsDeleted;
        }
    }
}