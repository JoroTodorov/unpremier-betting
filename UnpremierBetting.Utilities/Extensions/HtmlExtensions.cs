﻿using System;
using System.Globalization;
using System.Web.Mvc;

namespace UnpremierBetting.Utilities.Extensions
{
    public static class HtmlExtensions
    {
        public static MvcHtmlString DisplayPrice(this HtmlHelper helper, decimal price)
        {
            var format = new NumberFormatInfo() {NumberGroupSeparator = " "};
            string formatedPrice = price.ToString("N0", format);
          
            return new MvcHtmlString($"£{formatedPrice}");
        }

        public static MvcHtmlString MatchDate(this HtmlHelper helper, DateTime matchDate, string tag)
        {
            var h3 = new TagBuilder(tag);
            string matchDay = matchDate.ToString("D");
            h3.InnerHtml = matchDay;

            return new MvcHtmlString(h3.ToString());
        }

        public static MvcHtmlString TicketPanel(
            this HtmlHelper helper, string color, string match, string date, string odds, int panelNum)
        {
            var panel = new TagBuilder("div");
            panel.AddCssClass($"panel panel-{color}");

            var panelHeading = new TagBuilder("div");
            panelHeading.AddCssClass("panel-heading");

            var panelTitle = new TagBuilder("h4");
            panelTitle.AddCssClass("panel-title");

            var toggle = new TagBuilder("a");
            toggle.AddCssClass("accordion-toggle collapsed");
            toggle.MergeAttribute("data-toggle", "collapse");
            toggle.MergeAttribute("data-parent", "#accordion-panel");
            toggle.MergeAttribute("href", $"#panel-{panelNum}");
            toggle.MergeAttribute("aria-expanded", "false");
            toggle.InnerHtml = match;

            var panelContent = new TagBuilder("div");
            panelContent.AddCssClass("panel-collapse collapse");
            panelContent.MergeAttribute("id", $"panel-{panelNum}");
            panelContent.MergeAttribute("aria-expanded", "false");

            var panelBody = new TagBuilder("div");
            panelBody.AddCssClass("panel-body");
            
            var span = new TagBuilder("span");
            span.AddCssClass("label label-info pull-right");
            span.MergeAttribute("data-effect", "pop");
            span.InnerHtml = odds;

            panelBody.InnerHtml = date + span.ToString();
            panelContent.InnerHtml = panelBody.ToString();
            panelTitle.InnerHtml = toggle.ToString();
            panelHeading.InnerHtml = panelTitle.ToString();
            panel.InnerHtml = panelHeading + panelContent.ToString();

            return new MvcHtmlString(panel.ToString());
        }


    }
}