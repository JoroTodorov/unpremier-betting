﻿using System.Web.Mvc;

namespace UnpremierBetting.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName => "admin";

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.Routes.MapMvcAttributeRoutes();

            context.MapRoute(
                "admin",
                "admin/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new[] {"UnpremierBetting.Areas.Admin.Controllers"}
            );
        }
    }
}