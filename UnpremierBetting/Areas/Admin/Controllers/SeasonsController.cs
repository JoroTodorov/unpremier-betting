﻿using System;
using System.Linq;
using System.Web.Mvc;
using UnpremierBetting.Services.Interfaces;
using UnpremierBetting.Utilities.Attributes;

namespace UnpremierBetting.Areas.Admin.Controllers
{
    [AdminAuthorize]
    [RouteArea("admin")]
    [RoutePrefix("seasons")]
    public class SeasonsController : Controller
    {
        private readonly ISeasonsService service;

        public SeasonsController(ISeasonsService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route]
        public ActionResult Options()
        {
            this.ViewBag.AllSeasonYears = this.service.GetSeasonsList();
            this.ViewBag.AllLeaguesList = this.service.GetLeaguesList();
            return this.View();
        }

        [HttpPost]
        [Route("generate")]
        public ActionResult Generate(int leagueId, DateTime firstMatchDate)
        {
            if (this.ModelState.IsValid)
            {
                this.service.CreateSeasonMatches(leagueId, firstMatchDate);
                this.service.SimulateSeason(leagueId, firstMatchDate.Year);
            }

            return this.RedirectToAction("Options");
        }

        [HttpPost]
        [Route("delete")]
        public ActionResult Delete(int leagueId, int year)
        {
            this.service.DeleteSeasonMatches(leagueId, year);

            return this.RedirectToAction("Options");
        }

        [HttpPost]
        [Route("league")]
        public ActionResult League(string leagueName)
        {
            this.service.CreateLeague(leagueName);

            return this.RedirectToAction("Options");
        }
    }
}