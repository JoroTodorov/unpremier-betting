﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using UnpremierBetting.Models.BindingModels;
using UnpremierBetting.Services.Interfaces;
using UnpremierBetting.Utilities.Attributes;

namespace UnpremierBetting.Areas.Admin.Controllers
{
    [AdminAuthorize]
    [RouteArea("admin")]
    [RoutePrefix("clubs")]
    public class ClubsController : Controller
    {
        private readonly IClubsService service;

        public ClubsController(IClubsService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route]
        public ActionResult Options()
        {
            var clubVms = this.service.GetClubs();
            this.ViewBag.AllLeaguesList = this.service.GetLeaguesList();

            return this.View(clubVms);
        }

        [HttpPost]
        [Route("generate")]
        public ActionResult Generate(IEnumerable<int> ids)
        {
            this.service.CreateSquads(ids);

            return this.RedirectToAction("Options");
        }

        [HttpPost]
        [Route("delete")]
        public ActionResult Delete(IEnumerable<int> ids)
        {
            this.service.DeleteSquads(ids);

            return this.RedirectToAction("Options");
        }

        [HttpPost]
        [Route("add")]
        public ActionResult AddToLeague(LeagueBindingModel leagueBm)
        {
            this.service.AddClubToLeague(leagueBm);

            return this.RedirectToAction("Options");
        }
    }
}