﻿using System.Web.Mvc;

namespace UnpremierBetting.Areas.Admin.Controllers
{
    public class AdminController : Controller
    {
        [ChildActionOnly]
        public ActionResult AdminArea()
        {
            if (this.User.IsInRole("Administrator"))
            {
                return this.PartialView("_AdminArea");
            }

            return null;
        }
    }
}