﻿using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using UnpremierBetting.Models.BindingModels;
using UnpremierBetting.Models.ViewModels;
using UnpremierBetting.Models.ViewModels.Bets;
using UnpremierBetting.Services.Interfaces;

namespace UnpremierBetting.Areas.Bets.Controllers
{
    [Authorize]
    [RouteArea("bets")]
    public class BetsController : Controller
    {
        private readonly IBetsService service;

        public BetsController(IBetsService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("add/{betId}")]
        public ActionResult Add(int betId)
        {
            int matchId = this.service.GetMatchByBet(betId).Id;
            string userId = this.User.Identity.GetUserId();

            if (this.service.MatchIsBet(matchId, userId))
            {
                var errorVm = new PopupViewModel()
                {
                    Message = "Cannot add a bet on the same match.",
                    Type = "danger"
                };

                return this.PartialView("_Popup", errorVm);
            }

            this.service.AddMatchBet(betId, userId);

            var successVm = new PopupViewModel()
            {
                BoldMessage = "Brilliant!",
                Message = "Your bet was added to your ticket list.",
                Type = "success"
            };

            return this.PartialView("_Popup", successVm);
        }

        [HttpPost]
        [Route("remove/{betId}")]
        public ActionResult Remove(int betId)
        {
            var userId = this.User.Identity.GetUserId();
            this.service.RemoveBetFromTicket(betId, userId);

            var deletedVm = new PopupViewModel()
            {
                Message = "The bet was removed from the ticket list.",
                Type = "danger"
            };

            return this.PartialView("_Popup", deletedVm);
        }

        [Route("ticket")]
        [HttpGet]
        public ActionResult Ticket()
        {
            BetTicketViewModel ticketVm = this.GetUserTicket();

            return this.View(ticketVm);
        }

        [Route("ticket")]
        [HttpPost]
        public ActionResult Ticket(BetTicketBindingModel ticketBm)
        {
            string userId = this.User.Identity.GetUserId();
            BetTicketViewModel ticketVm = this.GetUserTicket();

            if (!this.service.UserHasTheAmount(userId, ticketBm.BettingAmount))
            {
                this.ModelState.AddModelError("BettingAmount", "Balance isn't enough for the bet amount");
            }
            if (!ticketVm.Bets.Any())
            {
                this.ModelState.AddModelError("Bets", "There should be at least one bet on the ticket list");
            }
            else if (ticketVm.Bets.Any(b => b.IsExpired))
            {
                this.ModelState.AddModelError("Bets", "Please remove the expired matches from the ticket list");
            }
            if (this.ModelState.IsValid)
            {
                BetTicketConfirmationViewModel tickerConfirmVm = this.service.GetTicketConfirmation(ticketBm);

                return this.RedirectToAction("Confirm", tickerConfirmVm);
            }

            return this.View(ticketVm);
        }

        [Route("confirm")]
        [HttpGet]
        public ActionResult Confirm(BetTicketConfirmationViewModel ticketVm)
        {
            return this.View(ticketVm);
        }

        [Route("confirm")]
        [HttpPost]
        public ActionResult Confirm(BetTicketConfirmationBindingModel ticketBm)
        {
            string userId = this.User.Identity.GetUserId();
            BetTicketViewModel ticketVm = this.GetUserTicket();

            if (!this.service.UserHasTheAmount(userId, ticketBm.BettingAmount) ||
                !ticketVm.Bets.Any() ||
                ticketVm.Bets.Any(b => b.IsExpired) ||
                !this.ModelState.IsValid)
            {
                var errorVm = new PopupViewModel()
                {
                    BoldMessage = "Error!",
                    Message = "Check your ticket list and resubmit again.",
                    Type = "danger"
                };

                return this.PartialView("_Popup", errorVm);
            }

            this.service.AddBetTicket(userId, ticketBm);

            var successVm = new PopupViewModel()
            {
                BoldMessage = "Brilliant!",
                Message = "Your ticket was submitted.",
                Type = "success"
            };

            return this.PartialView("_Popup", successVm);
        }

        [ChildActionOnly]
        public ActionResult Odds(int matchId)
        {
            string userId = this.User.Identity.GetUserId();
            if (this.service.MatchIsBet(matchId, userId))
            {
                return null;
            }

            var matchBets = this.service.GetMatchBets(matchId);
            return this.PartialView("_MatchBets", matchBets);
        }

        [ChildActionOnly]
        public ActionResult TicketBet(BetDetailsViewModel betVm, int panelNumber)
        {
            if (betVm.IsExpired)
            {
                betVm.PanelColor = "danger";
                betVm.ExpiredMessage = "(Expired)";
            }

            betVm.PanelNumber = panelNumber;
            return this.PartialView("_TicketBet", betVm);
        }

        private BetTicketViewModel GetUserTicket()
        {
            string userId = this.User.Identity.GetUserId();
            BetTicketViewModel ticketVm = this.service.GetUserTicket(userId);

            return ticketVm;
        }
    }
}