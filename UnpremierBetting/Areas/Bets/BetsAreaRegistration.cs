﻿using System.Web.Mvc;

namespace UnpremierBetting.Areas.Bets
{
    public class BetsAreaRegistration : AreaRegistration 
    {
        public override string AreaName => "bets";

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.Routes.MapMvcAttributeRoutes();

            context.MapRoute(
                "bets",
                "bets/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional },
                new[] { "UnpremierBetting.Areas.Bets.Controllers" }
            );
        }
    }
}