﻿using System.Web.Mvc;
using UnpremierBetting.Models.ViewModels.Players;
using UnpremierBetting.Services.Interfaces;

namespace UnpremierBetting.Controllers
{
    [RoutePrefix("players")]
    public class PlayersController : Controller
    {
        private readonly IPlayersService service;

        public PlayersController(IPlayersService service)
        {
            this.service = service;
        }

        [Route("{playerId}")]
        public ActionResult Details(int playerId)
        {
            PlayerDetailsViewModel playerVm = this.service.GetPlayer(playerId);

            this.ViewBag.LabelColor =
                playerVm.Overall < 40 ? "danger"
                    : playerVm.Overall < 70 ? "warning"
                        : playerVm.Overall < 90 ? "success" : "primary";

            return this.View(playerVm);
        }

        [ChildActionOnly]
        public ActionResult Stats(int leagueId = 0, int year = 0)
        {
            var statVms = this.service.GetStats(leagueId, year);

            return this.PartialView("_Stats", statVms);
        }
    }
}