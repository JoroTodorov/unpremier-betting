﻿using System.Web.Mvc;

namespace UnpremierBetting.Controllers
{
    public class HomeController : Controller
    {
        [Route]
        public ActionResult Index()
        {
            return this.View();
        }

        [ChildActionOnly]
        public ActionResult LoginMenu()
        {
            if (this.User.Identity.IsAuthenticated)
            {
                return this.PartialView("_MenuAuthorized");
            }

            return this.PartialView("_MenuUnauthorized");
        }
    }
}