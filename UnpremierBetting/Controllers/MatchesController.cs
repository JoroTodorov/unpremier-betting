﻿using System.Linq;
using System.Web.Mvc;
using UnpremierBetting.Models.ViewModels.Matches;
using UnpremierBetting.Services.Interfaces;
using UnpremierBetting.Utilities;

namespace UnpremierBetting.Controllers
{
    [RoutePrefix("matches")]
    public class MatchesController : Controller
    {
        private readonly IMatchesService service;
        public MatchesController(IMatchesService service)
        {
            this.service = service;
        }

        [Route("fixtures/{leagueId:int?}/{year:int?}/{page:int?}")]
        public ActionResult Fixtures(int leagueId = 0, int year = 0, int page = 0)
        {
            var fixtureVms = this.service.GetFixtures(leagueId, year);

            this.ViewBag.PageCount = Paginator.GetPagesCount(fixtureVms.Count());
            this.ViewBag.ItemCount = Paginator.ItemsPerPage;
            this.ViewBag.CurrentPage = page == 0 ? Paginator.GetActiveFixturesPage(fixtureVms) : page;

            this.ViewBag.SeasonYear = fixtureVms.First().MatchDay.Year;
            this.ViewBag.AllSeasonYears = this.service.GetSeasonsList();

            this.ViewBag.AllLeaguesList = this.service.GetLeaguesList(l => l.Seasons.Any(s => !s.IsDeleted));

            var pageFixtureVms = Paginator.GetPageItems(fixtureVms, this.ViewBag.CurrentPage);

            return this.View(pageFixtureVms);
        }

        [Authorize]
        [Route("details/{matchId}")]
        public ActionResult Details(int matchId)
        {
            MatchDetailsViewModel matchVm = this.service.GetMatchDetails(matchId);

            return this.View(matchVm);
        }
    }
}