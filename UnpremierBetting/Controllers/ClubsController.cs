﻿using System.Web.Mvc;
using UnpremierBetting.Models.ViewModels.Clubs;
using UnpremierBetting.Services.Interfaces;

namespace UnpremierBetting.Controllers
{
    [RoutePrefix("clubs")]
    public class ClubsController : Controller
    {
        private readonly IClubsService service;

        public ClubsController(IClubsService service)
        {
            this.service = service;
        }

        [Route("{id}")]
        public ActionResult Details(int id)
        {
            var clubVm = this.service.GetClubDetails(id);

            return this.View(clubVm);
        }

        [ChildActionOnly]
        public ActionResult PlayerInfo(ClubPlayerViewModel playerVm)
        {
            this.ViewBag.LabelColor =
               playerVm.Overall < 40 ? "danger"
                   : playerVm.Overall < 70 ? "warning"
                       : playerVm.Overall < 90 ? "success" : "primary";

            return this.PartialView("_PlayerInfo", playerVm);
        }
    }
}