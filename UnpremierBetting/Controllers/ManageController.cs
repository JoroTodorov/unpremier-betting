﻿using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using UnpremierBetting.Models.Enums;
using UnpremierBetting.Models.ViewModels.Manage;
using UnpremierBetting.Services.Identity;
using UnpremierBetting.Services.Interfaces;

namespace UnpremierBetting.Controllers
{
    [Authorize]
    [RoutePrefix("profile")]
    public class ManageController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        private readonly IProfilesService service;

        public ManageController(IProfilesService service)
        {
            this.service = service;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [HttpGet]
        [Route]
        public  ActionResult Index(ManageMessageId? message)
        {
            this.ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess
                    ? "Your password has been changed."
                    : message == ManageMessageId.SetPasswordSuccess
                        ? "Your password has been set."
                        : message == ManageMessageId.Error
                            ? "An error has occurred."
                            : "";

            var userId = this.User.Identity.GetUserId();
            decimal userBalance = this.service.GetUserBalance(userId);
            var userTickets = this.service.GetUserTickets(userId);

            var model = new IndexViewModel
            {
                HasPassword = this.HasPassword(),
                Balance = userBalance,
                Tickets = userTickets
            };

            return View(model);
        }

        [HttpGet]
        [Route("changePassword")]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [Route("changePassword")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                return RedirectToAction("Index", new { Message = ManageMessageId.ChangePasswordSuccess });
            }
            AddErrors(result);
            return View(model);
        }

        [ChildActionOnly]
        public ActionResult TicketCheck(TicketStatus status, bool isExpired)
        {
            if (status == TicketStatus.Failed || !isExpired)
            {
                return null;
            }

            return this.PartialView("_TicketCheck");
        }

        [ChildActionOnly]
        public ActionResult Status(TicketStatus status)
        {
            string color = "warning";

            if (status == TicketStatus.Won)
            {
                color = "success";
            }
            else if (status == TicketStatus.Failed)
            {
                color = "danger";
            }

            this.ViewBag.Color = color;

            return this.PartialView("_TicketStatus", status);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _userManager != null)
            {
                _userManager.Dispose();
                _userManager = null;
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager => this.HttpContext.GetOwinContext().Authentication;

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            return user?.PasswordHash != null;
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            Error
        }

        #endregion
    }
}