﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using UnpremierBetting.Models.ViewModels.Seasons;
using UnpremierBetting.Services.Interfaces;

namespace UnpremierBetting.Controllers
{
    [RoutePrefix("seasons")]
    public class SeasonsController : Controller
    {
        private readonly ISeasonsService service;

        public SeasonsController(ISeasonsService service)
        {
            this.service = service;
        }

        [Route("table")]
        public ActionResult Table(int leagueId = 0, int year = 0)
        {
            IEnumerable<LeagueTableClubViewModel> clubTableVms = this.service.GetLeagueTable(leagueId, year);

            this.ViewBag.AllLeagues = this.service.GetLeaguesList(l => l.Seasons.Any(s => !s.IsDeleted));
            this.ViewBag.AllSeasonYears = this.service.GetSeasonsList();

            this.ViewBag.SelectedYear = year;
            this.ViewBag.SelectedLeague = leagueId;

            return this.View(clubTableVms);
        }

        [ChildActionOnly]
        public ActionResult TableClub(LeagueTableClubViewModel clubVm, int clubsCount)
        {
            this.ViewBag.Color = 
                clubVm.Position == 1 ? "bg-info"
                : clubVm.Position <= 4 ? "bg-success"
                : clubVm.Position >= clubsCount - 2 ? "bg-danger" : string.Empty;

            return this.PartialView("_TableClub", clubVm);
        }
    }
}