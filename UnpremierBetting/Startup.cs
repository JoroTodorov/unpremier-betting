﻿using Microsoft.Owin;
using Owin;
using UnpremierBetting;

[assembly: OwinStartup(typeof(Startup))]
namespace UnpremierBetting
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            this.ConfigureAuth(app);
        }
    }
}
