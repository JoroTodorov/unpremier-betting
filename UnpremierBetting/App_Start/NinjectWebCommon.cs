[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(UnpremierBetting.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(UnpremierBetting.NinjectWebCommon), "Stop")]

namespace UnpremierBetting
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;

    using UnpremierBetting.Core;
    using UnpremierBetting.Core.Interfaces;
    using UnpremierBetting.Data;
    using UnpremierBetting.Data.Interfaces;
    using UnpremierBetting.Services;
    using UnpremierBetting.Services.Interfaces;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            //Data
            kernel.Bind<IDbContext>().To<UnpremierBettingContext>();
            kernel.Bind<IUnpremierBettingData>().To<UnpremierBettingData>();

            //Core
            kernel.Bind<IPlayerFactory>().To<PlayerFactory>();
            kernel.Bind<IClubManager>().To<ClubManager>();
            kernel.Bind<IMatchSystem>().To<MatchSystem>();

            //Web
            kernel.Bind<IPlayersService>().To<PlayersService>();
            kernel.Bind<IClubsService>().To<ClubsService>();
            kernel.Bind<IBetsService>().To<BetsService>();
            kernel.Bind<IMatchesService>().To<MatchesService>();
            kernel.Bind<ISeasonsService>().To<SeasonsService>();
            kernel.Bind<IProfilesService>().To<ProfilesService>();
        }        
    }
}
