﻿(function () {
    $('#non-squad-selector').click(function () {
        if (this.checked) {
            $("#non-squad-list input:checkbox").prop('checked', true);
        } else {
            $("#non-squad-list input:checkbox").prop('checked', false);
        }
    });
    $('#squad-selector').click(function () {
        if (this.checked) {
            $('#squad-list input:checkbox').prop("checked", true);
            $('#hidden-list input:checkbox').prop("checked", true);
        } else {
            $('#squad-list input:checkbox').prop("checked", false);
            $('#hidden-list input:checkbox').prop("checked", false);
        }
    });
    $("#squad-list").find('input:checkbox').click(function () {
        if (this.checked) {
            $("#hidden-list input:checkbox")
                .filter((index, i) => i.value === this.value)
                .prop('checked', true);
        } else {
            $("#hidden-list input:checkbox")
                .filter((index, i) => i.value === this.value)
                .prop('checked', false);
        }
    });
})();